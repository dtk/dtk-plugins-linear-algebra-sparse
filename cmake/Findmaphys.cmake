#
# Module to find the library MAPHYS
#

#
# If MAPHYS is found, it will set the following variables. Otherwise, 
# MAPHYS_FOUND will be set to false
#
#  MAPHYS_FOUND        True if maphys is found
#  MAPHYS_LIBRARIES    maphys_librarie
#  MAPHYS_INCLUDE_DIR  where to find mph_defs_f.h
#  MAPHYS_INCLUDE_DIRS where to find mph_def.h and  maphys modules
#  MAPHYS_MODULES_DIR directory og maphys modules

set(MAPHYS_FOUND "NO")

#if MAPHYS_DIR is specified
if(MAPHYS_DIR)
  find_path (MAPHYS_INCLUDE_DIR 
    NAMES mph_macros_f.h
    PATHS ${MAPHYS_DIR}/include
    NO_DEFAULT_PATH)

  find_path(MAPHYS_LIBRARY_DIR 
    NAMES libmaphys.a libmaphys.so
    PATHS ${MAPHYS_DIR}/lib 
    NO_DEFAULT_PATH)

  find_path(MAPHYS_MODULES_DIR 
    NAMES mph_common.mod
    PATHS ${MAPHYS_DIR}/modules 
    NO_DEFAULT_PATH)
endif()

# otherwise look for standard places 
find_path (MAPHYS_INCLUDE_DIR 
  NAMES mph_defs_f.h 
  PATHS  
  /usr/local/maphys/include
  /usr/local/include
  /opt/maphys/include
  /usr/include/maphys
  /usr/include
  ~/lib/include)

find_path(MAPHYS_LIBRARY_DIR
  NAMES libmaphys.a libmaphys.so
  PATHS 
  /opt/maphys/lib
  /usr/local/maphys/lib
  /usr/local/lib
  /usr/lib
  ~/lib/lib)

find_path(MAPHYS_MODULES_DIR
  NAMES mph_common.mod
  PATHS 
  /opt/maphys/lib
  /usr/local/maphys/lib
  /usr/local/lib
  /usr/lib
  ~/lib/lib)

if(MAPHYS_INCLUDE_DIR AND MAPHYS_LIBRARY_DIR)
  set(MAPHYS_FOUND YES)

  find_library(MAPHYS_LIBRARY 
    NAMES maphys
    PATHS ${MAPHYS_LIBRARY_DIR} 
    NO_DEFAULT_PATH)

  find_library(MAPHYS_TOOLKIT_LIBRARY 
    NAMES toolkit
    PATHS ${MAPHYS_LIBRARY_DIR} 
    NO_DEFAULT_PATH)

  find_library(MAPHYS_PACKCG_LIBRARY 
    NAMES packcg
    PATHS ${MAPHYS_LIBRARY_DIR}
    NO_DEFAULT_PATH)

  find_library(MAPHYS_PACKGMRES_LIBRARY 
    NAMES packgmres
    PATHS ${MAPHYS_LIBRARY_DIR} 
    NO_DEFAULT_PATH)

#  find_library(MAPHYS_SLATEC_LIBRARY 
#    NAMES slatec
#    PATHS ${MAPHYS_LIBRARY_DIR} 
#    NO_DEFAULT_PATH)

  set(MAPHYS_LIBRARIES ${MAPHYS_LIBRARY} ${MAPHYS_TOOLKIT_LIBRARY} ${MAPHYS_PACKCG_LIBRARY} ${MAPHYS_PACKGMRES_LIBRARY} ${MAPHYS_SLATEC_LIBRARY})

  set(MAPHYS_INCLUDE_DIRS ${MAPHYS_INCLUDE_DIR} ${MAPHYS_INCLUDE_DIR}/../modules)
else()
    message( "maphys_dir = ${MAPHYS_DIR}")
    message( "maphys_include_dirs = ${MAPHYS_INCLUDE_DIR}")
    message( "maphys_library = ${MAPHYS_LIBRARY}")
    if(MAPHYS_FIND_REQUIRED)
      message(FATAL_ERROR "MAPHYS not found, please set MAPHYS_DIR to your MAPHYS install directory")
    endif()
endif()
