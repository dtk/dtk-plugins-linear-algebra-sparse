#
# Module to find the library metis 4.xx
#

#
# If metis is found, it will set the following variables. Otherwise, 
# METIS_FOUND will be set to false
#
#  METIS_FOUND        True if metis is found
#  METIS_LIBRARIES    METIS_librarie


set(METIS_FOUND "NO")

#if METIS_DIR is specified
if(METIS_DIR)
  find_path (METIS_INCLUDE_DIR 
    NAMES metis.h 
    PATHS 
    ${METIS_DIR}/include
    ${METIS_DIR}/Lib
     NO_DEFAULT_PATH)

 find_path(METIS_LIBRARY_DIR 
    NAMES libmetis.a libmetis.so
    PATHS 
    ${METIS_DIR}/lib
    ${METIS_DIR} 
    NO_DEFAULT_PATH)
endif()

# otherwise look for standard places 
find_path (METIS_INCLUDE_DIR 
  NAMES metis.h 
  PATHS  
  /usr/local/metis/include
  /usr/local/include
  /opt/metis/include
  /usr/include)

find_path(METIS_LIBRARY_DIR
  NAMES libmetis.a libmetis.so
  PATHS 
  /opt/metis/lib
  /usr/local/metis/lib
  /usr/local/lib
  /usr/lib)

if(METIS_LIBRARY_DIR)
  set(METIS_FOUND YES)

  find_library(METIS_LIBRARY 
    NAMES metis
    PATHS ${METIS_LIBRARY_DIR} 
    NO_DEFAULT_PATH)

  set(METIS_LIBRARIES ${METIS_LIBRARY})
  
  mark_as_advanced(METIS_LIBRARY_DIR METIS_LIBRARY)
else()
  if(METIS_FIND_REQUIRED)
    message( "METIS_library = ${METIS_LIBRARY}")
    message(FATAL_ERROR "metis not found, please set METIS_DIR to your metis install directory")
  endif()
endif()
