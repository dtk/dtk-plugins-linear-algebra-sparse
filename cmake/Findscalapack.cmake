#
# Module to find the library scalapack
#

#
# If scalapack is found, it will set the following variables. Otherwise,
# SCALAPACK_FOUND will be set to false
#
#  SCALAPACK_FOUND        True if scalapack is found
#  SCALAPACK_LIBRARIES    SCALAPACK_librarie


set(SCALAPACK_FOUND "NO")

#if SCALAPACK_DIR is specified

if(${CMAKE_C_COMPILER} MATCHES "icc")
  # Intel composer has everything,
  if($ENV{MKLROOT} MATCHES "composer")
    include_directories($ENV{MKLROOT}/include)
    if(${CMAKE_SYSTEM_PROCESSOR} MATCHES "x86_64")
      set(SCALAPACK_LIBRARY_DIR "$ENV{MKLROOT}/lib/intel64" PATH)
      if(OPENMP_FOUND)
        link_libraries(-L$ENV{MKLROOT}/lib/intel64 -mkl)
      else(OPENMP_FOUND)
        link_libraries(-L$ENV{MKLROOT}/lib/intel64 -mkl=sequential)
      endif(OPENMP_FOUND)
    else()
      set(SCALAPACK_LIBRARY_DIR "$ENV{MKLROOT}/lib/ia32" PATH)
      if(OPENMP_FOUND)
        link_libraries(-L$ENV{MKLROOT}/lib/ia32 -mkl)
      else()
        link_libraries(-L$ENV{MKLROOT}/lib/ia32 -mkl=sequential)
      endif()
    endif()
    set(HAVE_MKL TRUE)
  endif()
else()
  if(SCALAPACK_DIR)
    find_path(SCALAPACK_LIBRARY_DIR
      NAMES libscalapack.a libscalapack.so
      PATHS ${SCALAPACK_DIR}/lib
      NO_DEFAULT_PATH)
  endif()

  # otherwise look for standard places
  find_path(SCALAPACK_LIBRARY_DIR
    NAMES libscalapack.a libscalapack.so
    PATHS
    /opt/scalapack/lib
    /usr/local/scalapack/lib
    /usr/local/lib
    /usr/lib
    ~/lib/lib)
endif()

if(SCALAPACK_LIBRARY_DIR)
  set(SCALAPACK_FOUND YES)

  if(HAVE_MKL)
    find_library(MKL_CORE_LIBRARY
      NAMES mkl_core
      PATHS ${SCALAPACK_LIBRARY_DIR}
      NO_DEFAULT_PATH)

    find_library(MKL_INTEL_LIBRARY
      NAMES mkl_intel_lp64
      PATHS ${SCALAPACK_LIBRARY_DIR}
      NO_DEFAULT_PATH)
    find_library(MKL_INTEL_LIBRARY
      NAMES mkl_intel_lp64
      PATHS ${SCALAPACK_LIBRARY_DIR}
      NO_DEFAULT_PATH)

    find_library(MKL_BLACS_LIBRARY
      NAMES mkl_blacs_openmpi_lp64
      PATHS ${SCALAPACK_LIBRARY_DIR}
      NO_DEFAULT_PATH)

    find_library(M_LIBRARY
      NAMES m
      PATHS ${SCALAPACK_LIBRARY_DIR}
      )

    if(OPENMP_FOUND)
      find_library(MKL_THREAD_LIBRARY
        NAMES mkl_intel_thread
        PATHS ${SCALAPACK_LIBRARY_DIR}
        NO_DEFAULT_PATH)
      message("link with mkl thread")
    else(OPENMP_FOUND)
      find_library(MKL_THREAD_LIBRARY
        NAMES mkl_sequential
        PATHS ${SCALAPACK_LIBRARY_DIR}
        NO_DEFAULT_PATH)
      message("link with mkl sequential")
    endif(OPENMP_FOUND)

    find_library(SCALAPACK_LIBRARY
      NAMES mkl_scalapack_lp64
      PATHS ${SCALAPACK_LIBRARY_DIR}
      NO_DEFAULT_PATH)

    set(SCALAPACK_LIBRARIES ${MKL_INTEL_LIBRARY} ${MKL_THREAD_LIBRARY} ${SCALAPACK_LIBRARY} ${MKL_CORE_LIBRARY} ${MKL_BLACS_LIBRARY} ${M_LIBRARY} )

    mark_as_advanced(SCALAPACK_LIBRARY_DIR SCALAPACK_LIBRARY MKL_INTEL_LIBRARY MKL_THREAD_LIBRARY MKL_CORE_LIBRARY MKL_BLACS_LIBRARY M_LIBRARY)

  else()
    find_library(SCALAPACK_LIBRARY
      NAMES scalapack
      PATHS ${SCALAPACK_LIBRARY_DIR}
      NO_DEFAULT_PATH)

    set(SCALAPACK_LIBRARIES ${SCALAPACK_LIBRARY})

    mark_as_advanced(SCALAPACK_LIBRARY_DIR SCALAPACK_LIBRARY)
  endif()
else()
  if(SCALAPACK_FIND_REQUIRED)
    message( "SCALAPACK_library = ${SCALAPACK_LIBRARY}")
    message(FATAL_ERROR "scalapack not found, please set SCALAPACK_DIR to your scalapack install directory or set MKLROOT")
  endif()
endif()


