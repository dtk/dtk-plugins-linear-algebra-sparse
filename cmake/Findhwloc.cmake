#
# Module to find the library hwloc
#

#
# If hwloc is found, it will set the following variables. Otherwise, 
# HWLOC_FOUND will be set to false
#
#  HWLOC_FOUND        True if hwloc is found
#  HWLOC_LIBRARIES    HWLOC_librarie
#  HWLOC_INCLUDE_DIR  where to find hwloc.h


set(HWLOC_FOUND "NO")

#if HWLOC_DIR is specified
if(HWLOC_DIR)
  find_path (HWLOC_INCLUDE_DIR 
    NAMES hwloc.h 
    PATHS ${HWLOC_DIR}/include
    NO_DEFAULT_PATH)

  find_path(HWLOC_LIBRARY_DIR 
    NAMES libhwloc.so
    PATHS ${HWLOC_DIR}/lib 
    NO_DEFAULT_PATH)
endif()

# otherwise look for standard places 
find_path (HWLOC_INCLUDE_DIR 
  NAMES hwloc.h 
  PATHS  
  /usr/local/hwloc/include
  /usr/local/include
  /opt/hwloc/include
  /usr/include/hwloc
  /usr/include
  ~/lib/include)

find_path(HWLOC_LIBRARY_DIR
  NAMES libhwloc.so
  PATHS 
  /opt/hwloc/lib
  /usr/local/hwloc/lib
  /usr/local/lib
  /usr/lib
  ~/lib/lib)

if(HWLOC_INCLUDE_DIR AND HWLOC_LIBRARY_DIR)
  set(HWLOC_FOUND YES)

  find_library(HWLOC_LIBRARY 
    NAMES hwloc
    PATHS ${HWLOC_LIBRARY_DIR} 
    NO_DEFAULT_PATH)

  set(HWLOC_LIBRARIES ${HWLOC_LIBRARY})

  set(HWLOC_INCLUDE_DIRS ${HWLOC_INCLUDE_DIR})

  mark_as_advanced(HWLOC_INCLUDE_DIR HWLOC_LIBRARY_DIR HWLOC_LIBRARY)
else()
  if(HWLOC_FIND_REQUIRED)
    message( "HWLOC_include_dirs = ${HWLOC_INCLUDE_DIR}")
    message( "HWLOC_library = ${HWLOC_LIBRARY}")
    message(FATAL_ERROR "hwloc not found, please set HWLOC_DIR to your hwloc install directory")
  endif()
endif()
