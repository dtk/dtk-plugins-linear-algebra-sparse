#
# Module to find the library MUMPS
#

#
# If MUMPS is found, it will set the following variables. Otherwise, 
# MUMPS_FOUND will be set to false
#
#  MUMPS_FOUND        True if MUMPS is found
#  MUMPS_LIBRARIES    MUMPS_librarie
#  MUMPS_INCLUDE_DIR  where to find mumps_compat.h


set(MUMPS_FOUND "NO")

#if MUMPS_DIR is specified
if(MUMPS_DIR)
  find_path (MUMPS_INCLUDE_DIR 
    NAMES mumps_compat.h 
    PATHS ${MUMPS_DIR}/include
    NO_DEFAULT_PATH)

  find_path(MUMPS_LIBRARY_DIR 
    NAMES libmumps_common.a libmumps_common.so
    PATHS ${MUMPS_DIR}/lib 
    NO_DEFAULT_PATH)
endif()

# otherwise look for standard places 
find_path (MUMPS_INCLUDE_DIR 
  NAMES mumps_compat.h 
  PATHS  
  /usr/local/MUMPS/include
  /usr/local/include
  /opt/MUMPS/include
  /usr/include/MUMPS
  /usr/include
  ~/lib/include)

find_path(MUMPS_LIBRARY_DIR
  NAMES libmumps_common.a libmumps_common.so
  PATHS 
  /opt/MUMPS/lib
  /usr/local/MUMPS/lib
  /usr/local/lib
  /usr/lib
  ~/lib/lib)

if(MUMPS_INCLUDE_DIR AND MUMPS_LIBRARY_DIR)
  set(MUMPS_FOUND YES)

  find_library(MUMPS_COMMON_LIBRARY 
    NAMES mumps_common
    PATHS ${MUMPS_LIBRARY_DIR} 
    NO_DEFAULT_PATH)

  find_library(MUMPS_D_LIBRARY 
    NAMES dmumps
    PATHS ${MUMPS_LIBRARY_DIR} 
    NO_DEFAULT_PATH)

  find_library(MUMPS_PORD_LIBRARY 
    NAMES pord
    PATHS ${MUMPS_LIBRARY_DIR}
    NO_DEFAULT_PATH)

  set(MUMPS_LIBRARIES ${MUMPS_D_LIBRARY} ${MUMPS_COMMON_LIBRARY} ${MUMPS_PORD_LIBRARY})

  set(MUMPS_INCLUDE_DIRS ${MUMPS_INCLUDE_DIR})

  mark_as_advanced(MUMPS_INCLUDE_DIR MUMPS_LIBRARY_DIR MUMPS_D_LIBRARY MUMPS_PORD_LIBRARY MUMPS_COMMON_LIBRARY)
else()
  if(MUMPS_FIND_REQUIRED)
    message( "MUMPS_include_dirs = ${MUMPS_INCLUDE_DIR}")
    message( "MUMPS_library = ${MUMPS_LIBRARY}")
    message(FATAL_ERROR "MUMPS not found, please set MUMPS_DIR to your MUMPS install directory")
  endif()
endif()
