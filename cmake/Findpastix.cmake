#
# Module to find the library pastix
#

#
# If pastix is found, it will set the following variables. Otherwise, 
# PASTIX_FOUND will be set to false
#
#  PASTIX_FOUND        True if pastix is found
#  PASTIX_LIBRARIES    PASTIX_librarie
#  PASTIX_INCLUDE_DIR  where to find pastix.h


set(PASTIX_FOUND "NO")

#if PASTIX_DIR is specified
if(PASTIX_DIR)
  find_path (PASTIX_INCLUDE_DIR 
    NAMES murge.h 
    PATHS ${PASTIX_DIR}/include
    NO_DEFAULT_PATH)

  find_path(PASTIX_LIBRARY_DIR 
    NAMES libpastix.a libpastix.so
    PATHS ${PASTIX_DIR}/lib 
    NO_DEFAULT_PATH)
endif()

# otherwise look for standard places 
find_path (PASTIX_INCLUDE_DIR 
  NAMES murge.h 
  PATHS  
  /usr/local/pastix/include
  /usr/local/include
  /opt/pastix/include
  /usr/include/pastix
  /usr/include
  ~/lib/include)

find_path(PASTIX_LIBRARY_DIR
  NAMES libpastix.a libpastix.so
  PATHS 
  /opt/pastix/lib
  /usr/local/pastix/lib
  /usr/local/lib
  /usr/lib
  ~/lib/lib)

if(PASTIX_INCLUDE_DIR AND PASTIX_LIBRARY_DIR)
  set(PASTIX_FOUND YES)

  find_library(PASTIX_LIBRARY 
    NAMES pastix
    PATHS ${PASTIX_LIBRARY_DIR} 
    NO_DEFAULT_PATH)

  set(PASTIX_LIBRARIES ${PASTIX_LIBRARY})

  set(PASTIX_INCLUDE_DIRS ${PASTIX_INCLUDE_DIR})

  mark_as_advanced(PASTIX_INCLUDE_DIR PASTIX_LIBRARY PASTIX_LIBRARY_DIR)
else()
  if(PASTIX_FIND_REQUIRED)
    message( "PASTIX_include_dirs = ${PASTIX_INCLUDE_DIR}")
    message( "PASTIX_library = ${PASTIX_LIBRARY}")
    message(FATAL_ERROR "pastix not found, please set PASTIX_DIR to your pastix install directory")
  endif()
endif()
