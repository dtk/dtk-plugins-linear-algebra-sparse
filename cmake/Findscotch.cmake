#
# Module to find the library scotch
#

#
# If scotch is found, it will set the following variables. Otherwise, 
# SCOTCH_FOUND will be set to false
#
#  SCOTCH_FOUND        True if scotch is found
#  SCOTCH_LIBRARIES    SCOTCH_librarie
#  SCOTCH_INCLUDE_DIR  where to find scotch.h


set(SCOTCH_FOUND "NO")

#if SCOTCH_DIR is specified
if(SCOTCH_DIR)
  find_path (SCOTCH_INCLUDE_DIR 
    NAMES scotch.h 
    PATHS ${SCOTCH_DIR}/include
    NO_DEFAULT_PATH)

  find_path(SCOTCH_LIBRARY_DIR 
    NAMES libscotch.a libscotch.so
    PATHS ${SCOTCH_DIR}/lib 
    NO_DEFAULT_PATH)
endif()

# otherwise look for standard places 
find_path (SCOTCH_INCLUDE_DIR 
  NAMES scotch.h 
  PATHS  
  /usr/local/scotch/include
  /usr/local/include
  /opt/scotch/include
  /usr/include/scotch
  /usr/include
  ~/lib/include)

find_path(SCOTCH_LIBRARY_DIR
  NAMES libscotch.a libscotch.so
  PATHS 
  /opt/scotch/lib
  /usr/local/scotch/lib
  /usr/local/lib
  /usr/lib
  ~/lib/lib)

if(SCOTCH_INCLUDE_DIR AND SCOTCH_LIBRARY_DIR)
  set(SCOTCH_FOUND YES)

  find_library(SCOTCH_LIBRARY 
    NAMES scotch
    PATHS ${SCOTCH_LIBRARY_DIR} 
    NO_DEFAULT_PATH)

  find_library(SCOTCH_ERREXIT_LIBRARY 
    NAMES scotcherrexit
    PATHS ${SCOTCH_LIBRARY_DIR} 
    NO_DEFAULT_PATH)

#  find_library(SCOTCH_ESMUMPS_LIBRARY 
#    NAMES esmumps
#    PATHS ${SCOTCH_LIBRARY_DIR}
#    NO_DEFAULT_PATH)

  set(SCOTCH_LIBRARIES ${SCOTCH_LIBRARY} ${SCOTCH_ERREXIT_LIBRARY} ${SCOTCH_ESMUMPS_LIBRARY})

  set(SCOTCH_INCLUDE_DIRS ${SCOTCH_INCLUDE_DIR})

  mark_as_advanced(SCOTCH_INCLUDE_DIR SCOTCH_LIBRARY_DIR SCOTCH_LIBRARY SCOTCH_ERREXIT_LIBRARY SCOTCH_ESMUMPS_LIBRARY) 
else()
  if(SCOTCH_FIND_REQUIRED)
    message( "SCOTCH_include_dirs = ${SCOTCH_INCLUDE_DIR}")
    message( "SCOTCH_library = ${SCOTCH_LIBRARY}")
    message(FATAL_ERROR "scotch not found, please set SCOTCH_DIR to your scotch install directory")
  endif()
endif()
