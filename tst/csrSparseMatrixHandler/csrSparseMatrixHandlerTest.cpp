// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "csrSparseMatrixHandlerTest.h"

#include <dtkLinearAlgebraSparse>

void csrSparseMatrixHandlerTestCase::initTestCase(void)
{
    dtkLinearAlgebraSparseSettings linear_algebra_sparse_settings;
    linear_algebra_sparse_settings.beginGroup("linear-algebra-sparse");
    dtkLinearAlgebraSparse::pluginManager::initialize(linear_algebra_sparse_settings.value("plugins").toString());
    linear_algebra_sparse_settings.endGroup();
}

void csrSparseMatrixHandlerTestCase::init(void)
{
}

void csrSparseMatrixHandlerTestCase::testFactoryCreation(void)
{
    dtkSparseMatrixHandler<double> *h_d = dtkLinearAlgebraSparse::matrixHandler::pluginFactory<double>().create("csrSparseMatrixHandler");
    QVERIFY(h_d);
    QVERIFY(h_d->rowCount() == 0);
    QVERIFY(h_d->colCount() == 0);
    QVERIFY(h_d->nonZeroCount() == 0);

    dtkSparseMatrixHandler<float > *h_f = dtkLinearAlgebraSparse::matrixHandler::pluginFactory<float >().create("csrSparseMatrixHandler");
    QVERIFY(h_f);
    QVERIFY(h_f->rowCount() == 0);
    QVERIFY(h_f->colCount() == 0);
    QVERIFY(h_f->nonZeroCount() == 0);

    delete h_d;
    delete h_f;
}

void csrSparseMatrixHandlerTestCase::testMatrixFromHandler(void)
{
    // Create handler first then give it to the matrix.

    dtkSparseMatrix<double> mat0(dtkLinearAlgebraSparse::matrixHandler::pluginFactory<double>().create("csrSparseMatrixHandler"));
    QVERIFY(mat0.rowCount() == 0);
    QVERIFY(mat0.colCount() == 0);
    QVERIFY(mat0.nonZeroCount() == 0);

    // Create directly the matrix using the factory and the handler type;
    dtkSparseMatrix<double> *mat1 = dtkLinearAlgebraSparse::matrixFactory::create<double>("csrSparseMatrixHandler");
    QVERIFY(mat1->rowCount() == 0);
    QVERIFY(mat1->colCount() == 0);
    QVERIFY(mat1->nonZeroCount() == 0);

    delete mat1;
}

void csrSparseMatrixHandlerTestCase::testResize(void)
{
    QScopedPointer<dtkSparseMatrixHandler<double> > h_d(dtkLinearAlgebraSparse::matrixHandler::pluginFactory<double>().create("csrSparseMatrixHandler"));

    h_d->resize(11, 11);
    QVERIFY(h_d->rowCount() == 11);
    QVERIFY(h_d->colCount() == 11);
    QVERIFY(h_d->nonZeroCount() == 0);
}

void csrSparseMatrixHandlerTestCase::testIterator(void)
{
    dtkSparseMatrixHandler<double> *h_d = dtkLinearAlgebraSparse::matrixHandler::pluginFactory<double>().create("csrSparseMatrixHandler");
    h_d->resize(11, 11);
    h_d->append(0, 0, 1);
    h_d->append(0, 1, 2);
    h_d->append(0, 5, 6);
    h_d->append(0, 8, 9);
    h_d->append(0, 10, 11);

    QScopedPointer<dtkSparseMatrixIterator<double> > it(h_d->cbegin(0));
    QScopedPointer<dtkSparseMatrixIterator<double> > end(h_d->cend(0));

    while((*it) != (*end)) {
        qDebug() << **it;
        ++(*it);
    }

    dtkScopedSparseMatrixIterator<double> sit(h_d->cbegin(0));
    dtkScopedSparseMatrixIterator<double> send(h_d->cend(0));
    
    while(sit != send) {
        qDebug() << *sit;
        ++sit;
    }

    dtkSparseMatrix<double> mat(h_d);

    dtkSparseMatrix<double>::const_iterator cit = mat.cbegin();
    dtkSparseMatrix<double>::const_iterator cend = mat.cend(11);
    while(cit != cend) {
        qDebug() << *cit;
        ++cit;
    }
    dtkSparseMatrix<double>::const_iterator tit = cit;
}

void csrSparseMatrixHandlerTestCase::cleanupTestCase(void)
{
    
}

void csrSparseMatrixHandlerTestCase::cleanup(void)
{
    
}

DTKTEST_MAIN_NOGUI(csrSparseMatrixHandlerTest, csrSparseMatrixHandlerTestCase)

// 
// csrSparseMatrixHandlerTest.cpp ends here
