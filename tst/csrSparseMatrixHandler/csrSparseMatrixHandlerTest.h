// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkTest>

class csrSparseMatrixHandlerTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testFactoryCreation(void);
    void testMatrixFromHandler(void);
    void testResize(void);
    void testIterator(void);

private slots:
    virtual void cleanupTestCase(void);
    virtual void cleanup(void);
};


// 
// csrSparseMatrixHandlerTest.h ends here
