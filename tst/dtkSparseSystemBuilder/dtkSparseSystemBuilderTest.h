// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkTest>

class dtkSparseSystemBuilderTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testDistributedSystemCreation(void);

private slots:
    virtual void cleanupTestCase(void);
    virtual void cleanup(void);
};


// 
// dtkSparseSystemBuilderTest.h ends here
