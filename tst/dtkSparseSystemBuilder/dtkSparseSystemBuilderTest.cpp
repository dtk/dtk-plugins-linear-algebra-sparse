// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkSparseSystemBuilderTest.h"

#include <dtkLinearAlgebraSparse>

void dtkSparseSystemBuilderTestCase::initTestCase(void)
{
}

void dtkSparseSystemBuilderTestCase::init(void)
{
}

void dtkSparseSystemBuilderTestCase::testDistributedSystemCreation(void)
{

    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel("trace");
    dtkDistributed::communicator::pluginManager().setVerboseLoading(true);
    dtkLinearAlgebraSparse::pluginManager::setVerboseLoading(true);

    auto system_builder = dtkSparseSystemBuilder<double>::instance();
    system_builder->setAutoLoading(false);
    system_builder->init("hypreSparseSolverPCG");

    auto solver = system_builder->createSolver();
    QVERIFY(solver);

    auto matrix = system_builder->createMatrix();
    QVERIFY(matrix);
    QVERIFY(matrix->dataType() == "dtkDistributedSparseMatrixEngineCSR");

    auto rhs = system_builder->createVector();
    QVERIFY(rhs);
    QVERIFY(rhs->dataType() == "dtkDistributedVectorData");

    delete solver;
    delete matrix;
    delete rhs;
}

void dtkSparseSystemBuilderTestCase::cleanupTestCase(void)
{

}

void dtkSparseSystemBuilderTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(dtkSparseSystemBuilderTest, dtkSparseSystemBuilderTestCase)

//
// dtkSparseSystemBuilderTest.cpp ends here
