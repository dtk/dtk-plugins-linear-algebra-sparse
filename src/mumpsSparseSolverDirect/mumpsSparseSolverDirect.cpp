// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "mumpsSparseSolverDirect.h"

#include <dmumps_c.h>
#include <mpi.h>

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

class mumpsSparseSolverDirectPrivate
{
public:
    dtkSparseMatrix<double> *matrix;
    dtkArray<double> *vector_rhs;
    dtkArray<double> *vector_solution;

public:
    DMUMPS_STRUC_C id;
};

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

mumpsSparseSolverDirect::mumpsSparseSolverDirect(void) : dtkSparseSolver<double>(), d(new mumpsSparseSolverDirectPrivate)
{
    d->matrix = NULL;
    d->vector_rhs = NULL;
    d->vector_solution = NULL;

    d->id.irn = NULL;
    d->id.jcn = NULL;
    d->id.a   = NULL;
}

mumpsSparseSolverDirect::~mumpsSparseSolverDirect(void)
{
    if (d->vector_solution)
        delete d->vector_solution;

    if (d->id.irn)
        delete d->id.irn;
    
    if (d->id.jcn)
        delete d->id.jcn;

    delete d;
}

void mumpsSparseSolverDirect::setMatrix(const dtkSparseMatrix<double>& matrix)
{
    d->matrix = &const_cast<dtkSparseMatrix<double>&>(matrix);
}

void mumpsSparseSolverDirect::setRHSVector(const dtkArray<double>& rhs_vector)
{
    d->vector_rhs = &const_cast<dtkArray<double>&>(rhs_vector);
}

void mumpsSparseSolverDirect::setOptionalParameters(const dtkArray<double>& parameters)
{
}

void mumpsSparseSolverDirect::setPreconditioner(const dtkSparsePreconditioner<double>& preconditioner)
{
    Q_UNUSED(preconditioner);
}

#define JOB_INIT -1
#define JOB_END -2
#define USE_COMM_WORLD -987654
#define ICNTL(I) icntl[(I)-1]

int mumpsSparseSolverDirect::run(void)
{
    // Initialize MUMPS

    int myid;
    int ierr;

    int initialized = 0;
    MPI_Initialized(&initialized);
    if (!initialized)
        MPI_Init(NULL, NULL);

    ierr = MPI_Comm_rank(MPI_COMM_WORLD, &myid);

    d->id.job = -1; 
    d->id.par = 1; 
    d->id.sym = 0;
    d->id.comm_fortran = USE_COMM_WORLD;
    dmumps_c(&(d->id));

    // Fill mumps struc params on the master

    if (!myid) {

        d->id.n  = d->matrix->rowCount();
        d->id.nz = d->matrix->nonZeroCount();

        if (!d->vector_solution)
            d->vector_solution = new dtkArray<double>(d->id.n);

        if (!d->id.irn) {
            d->id.irn = new int[d->id.nz];
            d->id.jcn = new int[d->id.nz];
            int count = 0;
            QPair<qlonglong, qlonglong> coords;
        
            // Loop over each row of the matrix
            for (int i = 0; i < d->id.n; ++i) {
                dtkSparseMatrix<double>::const_iterator cit  = d->matrix->cbegin(i);
                dtkSparseMatrix<double>::const_iterator cend = d->matrix->cend(i);
                // Loop over each non zero coef of the current row
                for(;cit != cend; ++cit) {
                    coords = cit.coordinates();
                    d->id.irn[count] = coords.first  + 1; // add 1 to fit with fortran numbering
                    d->id.jcn[count] = coords.second + 1; // add 1 to fit with fortran numbering
                    ++count;
                }
            }
        }
        
        // Retrieve matrix buffer to feed mumps data
        d->id.a = d->matrix->rawData();

        //id.rhs = new double[id.n];
        for (int i = 0; i < d->id.n; ++i) {
            (*d->vector_solution)[i] = (*d->vector_rhs)[i];
        }
        d->id.rhs = &((*d->vector_solution)[0]);
    }

    d->id.icntl[1-1] = -1; 
    d->id.icntl[2-1] = -1; 
    d->id.icntl[3-1] = -1; 
    d->id.icntl[4-1] =  0;

    d->id.job = 6;
    dmumps_c(&(d->id));

    d->id.job = JOB_END; 
    dmumps_c(&(d->id)); 

    return 1;
}

const dtkArray<double>& mumpsSparseSolverDirect::solutionVector(void) const
{
    return *(d->vector_solution);
}

// 
// mumpsSparseSolverDirect.cpp ends here
