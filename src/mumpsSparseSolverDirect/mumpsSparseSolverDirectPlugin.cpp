// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "mumpsSparseSolverDirect.h"
#include "mumpsSparseSolverDirectPlugin.h"

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

dtkSparseSolver<double>* mumpsSparseSolverDirectCreator(void);

// ///////////////////////////////////////////////////////////////////
// mumpsSparseSolverDirectPlugin
// ///////////////////////////////////////////////////////////////////

void mumpsSparseSolverDirectPlugin::initialize(void)
{
    dtkLinearAlgebraSparse::solver::pluginFactory<double>().record("mumpsSparseSolverDirect", mumpsSparseSolverDirectCreator);
}

void mumpsSparseSolverDirectPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(mumpsSparseSolverDirect)

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

dtkSparseSolver<double>* mumpsSparseSolverDirectCreator(void)
{
    return new mumpsSparseSolverDirect();
}

// 
// mumpsSparseSolverDirectPlugin.cpp ends here
