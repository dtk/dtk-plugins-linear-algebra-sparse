// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:


#pragma once

#include <dtkCore>

#include <dtkSparseSolver.h>

class mumpsSparseSolverDirectPlugin : public dtkSparseSolverPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkSparseSolverPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.mumpsSparseSolverDirectPlugin" FILE "mumpsSparseSolverDirectPlugin.json")

public:
     mumpsSparseSolverDirectPlugin(void) {}
    ~mumpsSparseSolverDirectPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

// 
// mumpsSparseSolverDirectPlugin.h ends here
