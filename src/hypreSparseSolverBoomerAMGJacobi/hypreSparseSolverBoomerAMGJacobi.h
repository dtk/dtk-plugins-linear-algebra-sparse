// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

class hypreSparseSolverBoomerAMGJacobiPrivate;

class hypreSparseSolverBoomerAMGJacobi : public dtkSparseSolver<double>
{
public:
     hypreSparseSolverBoomerAMGJacobi(void);
    ~hypreSparseSolverBoomerAMGJacobi(void);

public:
    void setMatrix(dtkSparseMatrix<double> *matrix);
    void setRHSVector(dtkVector<double> *rhs_vector);
    void setSolutionVector(dtkVector<double> *sol_vector);

    void setOptionalParameters(const QHash<QString, int>& parameters);
    void setOptionalParameters(const QHash<QString, double>& parameters);

public:
    void run(void);

public:
    dtkVector<double> *solutionVector(void) const;
    const dtkArray<double>& stats(void) const;

protected:
    hypreSparseSolverBoomerAMGJacobiPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkSparseSolver<double> *hypreSparseSolverBoomerAMGJacobiCreator(void)
{
    return new hypreSparseSolverBoomerAMGJacobi();
}

//
// hypreSparseSolverBoomerAMGJacobi.h ends here
