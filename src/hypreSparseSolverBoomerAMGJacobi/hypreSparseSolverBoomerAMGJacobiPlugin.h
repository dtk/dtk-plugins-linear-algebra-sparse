// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkCore>

#include <dtkSparseSolver.h>

class hypreSparseSolverBoomerAMGJacobiPlugin : public dtkSparseSolverPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkSparseSolverPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.hypreSparseSolverBoomerAMGJacobiPlugin" FILE "hypreSparseSolverBoomerAMGJacobiPlugin.json")

public:
     hypreSparseSolverBoomerAMGJacobiPlugin(void) {}
    ~hypreSparseSolverBoomerAMGJacobiPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// hypreSparseSolverBoomerAMGJacobiPlugin.h ends here
