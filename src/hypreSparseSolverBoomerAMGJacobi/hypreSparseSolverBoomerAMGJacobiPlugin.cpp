// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "hypreSparseSolverBoomerAMGJacobi.h"
#include "hypreSparseSolverBoomerAMGJacobiPlugin.h"

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

// ///////////////////////////////////////////////////////////////////
// hypreSparseSolverBoomerAMGJacobiPlugin
// ///////////////////////////////////////////////////////////////////

void hypreSparseSolverBoomerAMGJacobiPlugin::initialize(void)
{
    dtkLinearAlgebraSparse::solver::pluginFactory<double>().record("hypreSparseSolverBoomerAMGJacobi", hypreSparseSolverBoomerAMGJacobiCreator);
}

void hypreSparseSolverBoomerAMGJacobiPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(hypreSparseSolverBoomerAMGJacobi)

//
// hypreSparseSolverBoomerAMGJacobiPlugin.cpp ends here
