// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "hypreSparseSolverBoomerAMGJacobi.h"

#include <mpi.h>
#include <HYPRE.h>
#include <HYPRE_utilities.h>
#include <HYPRE_parcsr_ls.h>
#include <HYPRE_IJ_mv.h>
#include <HYPRE_krylov.h>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class hypreSparseSolverBoomerAMGJacobiPrivate
{
public:
    dtkSparseMatrix<double> *matrix;
    dtkVector<double> *rhs_vector;
    dtkVector<double> *sol_vector;

public:
    QHash<QString, int> params_int;
    QHash<QString, double> params_real;

    int number_of_iterations;
    int state_size;
    double residual_reduction_order;

public:
    dtkArray<double> stats;

public:
    bool A_initialized;
    bool b_initialized;
    bool x_initialized;

public:
    HYPRE_IJMatrix ij_A;
    HYPRE_ParCSRMatrix par_A;
    HYPRE_Int *rows;
    HYPRE_Int nrows;
    HYPRE_Int *ncols;
    HYPRE_Int *cols;
    double *values_A;

    HYPRE_IJVector ij_b;
    HYPRE_ParVector par_b;
    HYPRE_Int *b_rows;
    HYPRE_Int b_nrows;
    double *values_b;

    HYPRE_IJVector ij_x;
    HYPRE_ParVector par_x;
    double *values_x;

    HYPRE_Solver par_solver;
    HYPRE_Solver precond;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

hypreSparseSolverBoomerAMGJacobi::hypreSparseSolverBoomerAMGJacobi(void) : dtkSparseSolver<double>(), d(new hypreSparseSolverBoomerAMGJacobiPrivate)
{
    d->matrix = NULL;
    d->rhs_vector = NULL;
    d->sol_vector = NULL;

    d->residual_reduction_order = 1.e-5;
    d->number_of_iterations = 50;
    d->state_size = 1;

    d->A_initialized = false;
    d->b_initialized = false;
    d->x_initialized = false;

    d->rows = NULL;
    d->ncols = NULL;
    d->cols = NULL;
    d->values_A = NULL;

    d->values_b = NULL;
    d->b_rows = NULL;

    d->values_x = NULL;

    HYPRE_BoomerAMGCreate(&(d->par_solver));
}

hypreSparseSolverBoomerAMGJacobi::~hypreSparseSolverBoomerAMGJacobi(void)
{
    HYPRE_BoomerAMGDestroy(d->par_solver);

    if (d->A_initialized)
        HYPRE_ParCSRMatrixDestroy(d->par_A);
    if (d->b_initialized)
        HYPRE_ParVectorDestroy(d->par_b);
    if (d->x_initialized)
        HYPRE_ParVectorDestroy(d->par_x);

    if (d->rows)
        delete[] d->rows;
    if (d->ncols)
        delete[] d->ncols;
    if (d->cols)
        delete[] d->cols;
    if (d->b_rows)
        delete[] d->b_rows;

    delete d;
}

void hypreSparseSolverBoomerAMGJacobi::setMatrix(dtkSparseMatrix<double> *matrix)
{
    d->matrix = matrix;

    if (!d->A_initialized) {
        d->A_initialized = true;

        dtkSparseMatrix<double>::line_iterator l_it  = d->matrix->begin();
        dtkSparseMatrix<double>::line_iterator l_end = d->matrix->end();

        HYPRE_Int ilower = l_it.id();
        HYPRE_Int iupper = (--l_end).id(); ++l_end;
        HYPRE_Int jlower = ilower;
        HYPRE_Int jupper = iupper;
        d->nrows = iupper - ilower + 1;

        d->rows  = new HYPRE_Int[d->nrows];
        d->ncols = new HYPRE_Int[d->nrows];
        HYPRE_Int nnz = 0;
        for(HYPRE_Int i = 0; l_it != l_end; ++l_it, ++i) {
            d->rows[i] = l_it.id();
            d->ncols[i] = l_it.size();
            nnz += l_it.size();
        }
        d->cols = new HYPRE_Int[nnz];
        nnz = 0;
        l_it  = d->matrix->begin();
        for(; l_it != l_end; ++l_it) {
            dtkSparseMatrixLineElement<double> elt = l_it.begin();
            dtkSparseMatrixLineElement<double> end = l_it.end();
            for(; elt != end; ++elt) {
                d->cols[nnz] = elt.id();
                ++nnz;
            }
        }

        HYPRE_IJMatrixCreate(MPI_COMM_WORLD, ilower, iupper, jlower, jupper, &(d->ij_A));
        HYPRE_IJMatrixSetObjectType(d->ij_A, HYPRE_PARCSR);
        HYPRE_IJMatrixInitialize(d->ij_A);
    }

    d->values_A = d->matrix->data();

    HYPRE_IJMatrixSetValues(d->ij_A, d->nrows, d->ncols, d->rows, d->cols, d->values_A);
    HYPRE_IJMatrixAssemble(d->ij_A);
}

void hypreSparseSolverBoomerAMGJacobi::setRHSVector(dtkVector<double> *rhs_vector)
{
    d->rhs_vector = rhs_vector;

    if (!d->b_initialized) {
        d->b_initialized = true;

        dtkVector<double>::iterator it  = d->rhs_vector->begin();
        dtkVector<double>::iterator end = d->rhs_vector->end();

        HYPRE_Int jlower = it.id();
        HYPRE_Int jupper = (--end).id(); ++end;

        d->b_nrows = jupper - jlower + 1;

        if (!d->b_rows) {
            d->b_rows  = new HYPRE_Int[d->b_nrows];
            for(HYPRE_Int i = 0; it != end; ++it, ++i) {
                d->b_rows[i] = it.id();
            }
        }

        HYPRE_IJVectorCreate(MPI_COMM_WORLD, jlower, jupper, &d->ij_b);
        HYPRE_IJVectorSetObjectType(d->ij_b, HYPRE_PARCSR);
        HYPRE_IJVectorInitialize(d->ij_b);
    }

    d->values_b = d->rhs_vector->data();

    HYPRE_IJVectorSetValues(d->ij_b, d->b_nrows, d->b_rows, d->values_b);
    HYPRE_IJVectorAssemble(d->ij_b);
}

void hypreSparseSolverBoomerAMGJacobi::setSolutionVector(dtkVector<double> *sol_vector)
{
    d->sol_vector = sol_vector;

    if (!d->x_initialized) {
        d->x_initialized = true;

        dtkVector<double>::iterator it  = d->sol_vector->begin();
        dtkVector<double>::iterator end = d->sol_vector->end();

        HYPRE_Int jlower = it.id();
        HYPRE_Int jupper = (--end).id(); ++end;

        d->b_nrows = jupper - jlower + 1;

        if (!d->b_rows) {
            d->b_rows = new HYPRE_Int[d->b_nrows];
            for(HYPRE_Int i = 0; it != end; ++it, ++i) {
                d->b_rows[i] = it.id();
            }
        }

        HYPRE_IJVectorCreate(MPI_COMM_WORLD, jlower, jupper, &d->ij_x);
        HYPRE_IJVectorSetObjectType(d->ij_x, HYPRE_PARCSR);
        HYPRE_IJVectorInitialize(d->ij_x);
    }

    d->values_x = d->sol_vector->data();

    HYPRE_IJVectorSetValues(d->ij_x, d->b_nrows, d->b_rows, d->values_x);
    HYPRE_IJVectorAssemble(d->ij_x);
}

void hypreSparseSolverBoomerAMGJacobi::setOptionalParameters(const QHash<QString, int>& parameters)
{
     QHash<QString, int>::const_iterator it = parameters.find("number_of_iterations");

     if(it != parameters.end())
         d->params_int.insert("number_of_iterations", it.value());

     d->number_of_iterations = d->params_int.value("number_of_iterations");

     it = parameters.find("state_size");

     if(it != parameters.end())
         d->params_int.insert("state_size", it.value());

     d->state_size = d->params_int.value("state_size");
}

void hypreSparseSolverBoomerAMGJacobi::setOptionalParameters(const QHash<QString, double>& parameters)
{
     QHash<QString, double>::const_iterator it = parameters.find("residual_reduction_order");

     if(it != parameters.end())
         d->params_real.insert("residual_reduction_order", it.value());

     d->residual_reduction_order = d->params_real.value("residual_reduction_order");
}

void hypreSparseSolverBoomerAMGJacobi::run(void)
{
    if (!d->x_initialized) {
        this->setSolutionVector(new dtkVector<double>(*(d->rhs_vector)));
    }

    HYPRE_IJMatrixGetObject(d->ij_A, (void **) &(d->par_A));
    HYPRE_IJVectorGetObject(d->ij_b, (void **) &(d->par_b));
    HYPRE_IJVectorGetObject(d->ij_x, (void **) &(d->par_x));

    // set the solver parameters
    HYPRE_BoomerAMGSetMaxIter(d->par_solver, d->number_of_iterations);
    HYPRE_BoomerAMGSetTol(d->par_solver, d->residual_reduction_order);
    HYPRE_BoomerAMGSetNumFunctions(d->par_solver, d->state_size);

    HYPRE_BoomerAMGSetMaxLevels(d->par_solver, 25);
    HYPRE_BoomerAMGSetNumSweeps(d->par_solver, 3);

    HYPRE_BoomerAMGSetPrintLevel(d->par_solver, 3); // print amg solution info
    HYPRE_BoomerAMGSetRelaxType(d->par_solver, 0); // Jacobi simple
    HYPRE_BoomerAMGSetRelaxWt(d->par_solver, 0); // automatic relaxation weight determination
    HYPRE_BoomerAMGSetStrongThreshold(d->par_solver, 0.7);



    // HYPRE_BoomerAMGSetCoarsenType(d->par_solver, 6);
    // HYPRE_BoomerAMGSetCycleType(d->par_solver, 2);
    // HYPRE_BoomerAMGSetRelaxWt(d->par_solver, 0.3);
    // HYPRE_BoomerAMGSetInterpType(d->par_solver, 10);
    // HYPRE_BoomerAMGSetNodal(d->par_solver, 1);
    // HYPRE_BoomerAMGSetNodalDiag(d->par_solver, 1);
    // do the setup and solve the system using the solver
    HYPRE_BoomerAMGSetup(d->par_solver, d->par_A, d->par_b, d->par_x);
    HYPRE_BoomerAMGSolve(d->par_solver, d->par_A, d->par_b, d->par_x);

    // get the local solution
    HYPRE_IJVectorGetValues(d->ij_x, d->nrows, d->rows, d->values_x);
}

dtkVector<double> *hypreSparseSolverBoomerAMGJacobi::solutionVector(void) const
{
    return d->sol_vector;
}

const dtkArray<double>& hypreSparseSolverBoomerAMGJacobi::stats(void) const
{
    return d->stats;
}

//
// hypreSparseSolverBoomerAMGJacobi.cpp ends here
