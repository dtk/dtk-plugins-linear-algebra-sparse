// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

class hypreSparseSolverDistributedPrivate;

class hypreSparseSolverDistributed : public dtkSparseSolver<double>
{
public:
     hypreSparseSolverDistributed(void);
    ~hypreSparseSolverDistributed(void);

public:
    void setMatrix(dtkSparseMatrix<double> *matrix);
    void setRHSVector(dtkVector<double> *rhs_vector);
    void setSolutionVector(dtkVector<double> *sol_vector);

    void setOptionalParameters(const QHash<QString, int>& parameters);
    void setOptionalParameters(const QHash<QString, double>& parameters);

public:
    void run(void);

public:
    dtkVector<double> *solutionVector(void) const;
    const dtkArray<double>& stats(void) const;

protected:
    hypreSparseSolverDistributedPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkSparseSolver<double> *hypreSparseSolverDistributedCreator(void)
{
    return new hypreSparseSolverDistributed();
}

// 
// hypreSparseSolverDistributed.h ends here
