// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "hypreSparseSolverDistributed.h"
#include "hypreSparseSolverDistributedPlugin.h"

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

// ///////////////////////////////////////////////////////////////////
// hypreSparseSolverDistributedPlugin
// ///////////////////////////////////////////////////////////////////

void hypreSparseSolverDistributedPlugin::initialize(void)
{
    dtkLinearAlgebraSparse::solver::pluginFactory<double>().record("hypreSparseSolverDistributed", hypreSparseSolverDistributedCreator);
}

void hypreSparseSolverDistributedPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(hypreSparseSolverDistributed)

// 
// hypreSparseSolverDistributedPlugin.cpp ends here
