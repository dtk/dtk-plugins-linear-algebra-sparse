// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:


#pragma once

#include <dtkCore>

#include <dtkSparseSolver.h>

class hypreSparseSolverDistributedPlugin : public dtkSparseSolverPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkSparseSolverPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.hypreSparseSolverDistributedPlugin" FILE "hypreSparseSolverDistributedPlugin.json")

public:
     hypreSparseSolverDistributedPlugin(void) {}
    ~hypreSparseSolverDistributedPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

// 
// hypreSparseSolverDistributedPlugin.h ends here
