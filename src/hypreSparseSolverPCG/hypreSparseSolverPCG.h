// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

class hypreSparseSolverPCGPrivate;

class hypreSparseSolverPCG : public dtkSparseSolver<double>
{
public:
     hypreSparseSolverPCG(void);
    ~hypreSparseSolverPCG(void);

public:
    void setMatrix(dtkSparseMatrix<double>* matrix);
    void setRHSVector(dtkVector<double>* rhs_vector);
    void setSolutionVector(dtkVector<double>* sol_vector);

    void setOptionalParameters(const QHash<QString, double>& parameters);
    void setOptionalParameters(const QHash<QString, int>& parameters);

public:
    void run(void);

public:
    dtkVector<double>* solutionVector(void) const;
    const dtkArray<double>& stats(void) const;

protected:
    hypreSparseSolverPCGPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkSparseSolver<double>* hypreSparseSolverPCGCreator(void)
{
    return new hypreSparseSolverPCG();
}

// 
// hypreSparseSolverPCG.h ends here
