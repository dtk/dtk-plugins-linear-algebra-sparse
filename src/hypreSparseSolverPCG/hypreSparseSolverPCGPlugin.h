// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:


#pragma once

#include <dtkCore>

#include <dtkSparseSolver.h>

class hypreSparseSolverPCGPlugin : public dtkSparseSolverPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkSparseSolverPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.hypreSparseSolverPCGPlugin" FILE "hypreSparseSolverPCGPlugin.json")

public:
     hypreSparseSolverPCGPlugin(void) {}
    ~hypreSparseSolverPCGPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

// 
// hypreSparseSolverPCGPlugin.h ends here
