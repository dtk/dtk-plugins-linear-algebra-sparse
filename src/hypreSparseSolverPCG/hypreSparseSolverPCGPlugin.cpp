// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "hypreSparseSolverPCG.h"
#include "hypreSparseSolverPCGPlugin.h"

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

// ///////////////////////////////////////////////////////////////////
// hypreSparseSolverPCGPlugin
// ///////////////////////////////////////////////////////////////////

void hypreSparseSolverPCGPlugin::initialize(void)
{
    dtkLinearAlgebraSparse::solver::pluginFactory<double>().record("hypreSparseSolverPCG", hypreSparseSolverPCGCreator);
}

void hypreSparseSolverPCGPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(hypreSparseSolverPCG)

// 
// hypreSparseSolverPCGPlugin.cpp ends here
