// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QFile>
#include <QString>
#include <QTextStream>

#include <mpi.h>
#include "omp.h"
#include <dtkDistributed>

#include "maphysFEMSparseSolver.h"
#include "hwloc.h"

extern "C" {
#include "dmph_maphys_type_c.h"
}


#define MAPHYS_INIT -1
#define MAPHYS_END -2
#define MAPHYS_SOLVE 6
#define USE_COMM_WORLD -987654
#define ICNTL(I) icntl[(I)-1] /* macro s.t. indices match documentation */
#define RCNTL(I) rcntl[(I)-1] /* macro s.t. indices match documentation */
#define IINFO(I) iinfo[(I)-1] /* macro s.t. indices match documentation */
#define RINFO(I) rinfo[(I)-1] /* macro s.t. indices match documentation */
#define RINFOG(I) rinfog[(I)-1] /* macro s.t. indices match documentation */
#define IINFOG(I) iinfog[(I)-1] /* macro s.t. indices match documentation */
// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

#define MAPHYS_INT int

class maphysFEMSparseSolverPrivate
{
public:
    dtkSparseMatrix<double> *matrix;
    dtkVector<double> *rhs_vector;
    dtkVector<double> *sol_vector;

    bool reuse_facto;
    int number_of_iterations;
    double residual_reduction_order;

public:
    int mpi_rank;
    int mpi_size;
    dtkArray<double> stats;
    int number_of_cores;
    int number_of_nodes;

public:
    bool A_initialized;
    bool rhs_initialized;
    bool sol_initialized;

public:
    int *rows;
    int *cols;
    double *rhs_values;
    double *sol_values;
    int nnz;
    int nb_local_rows; // number of local rows in the matrix
    qlonglong nb_itrf_with_lower_proc; // number of interfaces with process of lower rank

public:
    DMPH_maphys_t_c id;
    const dtkArray<qlonglong, 0LL> *loc_to_glob;

public:
    MAPHYS_INT myndof;
    MAPHYS_INT myndofinterior;
    MAPHYS_INT myndofintrf;
    MAPHYS_INT mynbvi;
    MAPHYS_INT myndoflogicintrf;
    MAPHYS_INT *myindexvi;
    MAPHYS_INT *myptrindexvi;
    MAPHYS_INT *myindexintrf;
    MAPHYS_INT *myinterface;
public:
    int getNumberOfCores();
    int getNumberOfNodes();
    void initDistributedInterface();
};

int maphysFEMSparseSolverPrivate::getNumberOfCores(void)
{
    if(number_of_cores==0) {
	// Allocate, initialize, and perform topology detection
	hwloc_topology_t topology;
	hwloc_topology_init(&topology);
	hwloc_topology_load(topology);

	// Try to get the number of CPU cores from topology
	int depth = hwloc_get_type_depth(topology, HWLOC_OBJ_CORE);
	if(depth == HWLOC_TYPE_DEPTH_UNKNOWN) {
	    qDebug() << "*** The number of cores is unknown default 0";
	    number_of_cores=0;
	}
	else {
	    number_of_cores = hwloc_get_nbobjs_by_depth(topology, depth);
        //    qDebug() << "*** nb core(s)" << number_of_cores;
	}
	// Destroy topology object and return
	hwloc_topology_destroy(topology);
    }

    return number_of_cores;
}

int maphysFEMSparseSolverPrivate::getNumberOfNodes(void)
{
    if(number_of_nodes==0) {
	int rank, is_rank0;
	MPI_Comm shmcomm;

	MPI_Comm_split_type(MPI_COMM_WORLD, MPI_COMM_TYPE_SHARED, 0,
			    MPI_INFO_NULL, &shmcomm);
	MPI_Comm_rank(shmcomm, &rank);
	is_rank0 = (rank == 0) ? 1 : 0;
	MPI_Allreduce(&is_rank0, &number_of_nodes, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	MPI_Comm_free(&shmcomm);
    //	qDebug() << "number of nodes " << number_of_nodes;
    }

    return number_of_nodes;
}

void maphysFEMSparseSolverPrivate::initDistributedInterface(void)
{
    //get the DDData
    const dtkDistributedGraphTopology::DDData& dd = matrix->graph()->m_dd;
    loc_to_glob = &dd.loc_to_glob;
    //get the communicator
    dtkDistributedCommunicator *comm = dtkDistributed::communicator::instance();
    mpi_rank = comm->rank();
    mpi_size = comm->size();

// /////////////////////////////////////////////////////////////////
// create maphys arrays data
// /////////////////////////////////////////////////////////////////

    myndofinterior = dd.local_intern_size;
    myndofintrf = dd.local_itf_size;
    myndof = myndofintrf + myndofinterior;
    myndoflogicintrf = dd.map_itf[mpi_rank].size();
    mynbvi = dd.map_itf.size() -1;

    myindexvi = new MAPHYS_INT[mynbvi];
    myptrindexvi = new MAPHYS_INT[mynbvi+1];
    myinterface = new MAPHYS_INT[myndofintrf];
    {
        int lenindintrf=0;
        int cnt_indexvi=0;
        int cnt_interface=0;
        QList< int> id_of_interfaces;
        QList<MAPHYS_INT> myindexintrf_list;
        id_of_interfaces.reserve(myndofintrf);
        myindexintrf_list.reserve(myndofintrf);

        auto it = dd.map_itf.cbegin();
        auto ite = dd.map_itf.cend();
        for(; it != ite; ++it)
        {
            if( it.key() != mpi_rank) {
                myptrindexvi[cnt_indexvi] = lenindintrf +1;
                myindexvi[cnt_indexvi] = it.key();

                auto it_v = it.value().cbegin();
                auto ite_v =  it.value().cend();
                for(; it_v != ite_v; ++it_v)
                {
                    if(!id_of_interfaces.contains(*it_v)) {
                        id_of_interfaces.append(*it_v);
                        myinterface[cnt_interface++] = (*it_v)+1;
                    }
                    myindexintrf_list.append( dd.glob_to_loc[(*it_v)] + 1 - myndofinterior );
                    lenindintrf++;
                    // if(mpi_rank< it.key()) qDebug() << mpi_rank << "with"<< it.key() << (*it_v);
                }
                cnt_indexvi++;
            }
        }
        myptrindexvi[cnt_indexvi]=lenindintrf+1;
        //mysizeintrf = myndofintrf;

        myindexintrf = new MAPHYS_INT[myindexintrf_list.size()];
        for(int i=0; i<myindexintrf_list.size(); ++i)
            myindexintrf[i] = myindexintrf_list[i];

        // QString filename = "maphys_domain_" % QString("%1").arg(mpi_rank,4,10,QChar('0')) % ".txt";
        // QFile file(filename);
        // if (file.open(QIODevice::ReadWrite)) {
	//     QTextStream stream(&file);
	//     stream << " % myndof myndofinterior myndofinterface mynbvi " << endl;
	//     stream << myndof << " " << myndofinterior << " " << myndofintrf << " " << mynbvi << endl;

	//     stream << " % myindexvi " << endl;
	//     for(int i=0; i < mynbvi  ; ++i)
	// 	stream << myindexvi[i] << " ";
	//     stream << endl;

	//     stream << " % myptrindexVi " << endl;
	//     for(int i=0; i < mynbvi+1  ; ++i)
	// 	stream << myptrindexvi[i] << " ";
	//     stream << endl;

	//     stream << " % myindexintrf " << endl;
	//     for(int i=0; i < myindexintrf_list.size() ; ++i)
	// 	stream << myindexintrf[i] << " ";
	//     stream << endl;

	//     stream << " % myinterface " << endl;
	//     for(int i=0; i < myndofintrf  ; ++i)
	// 	stream << myinterface[i] << " ";
	//     stream << endl;

	//     stream << " % mylogicintrf " << endl;
	//     for(int i=0; i < myndoflogicintrf  ; ++i)
	// 	stream << mylogicintrf[i] << " ";
	//     stream << endl;

        // }


    }

  /*
    + mphs : the MAPHYS instance
    - myndof : integer equal to the degree of freedom of the entire domain (interface + interior) ;
   // - myndof interior : integer equal to the degree of freedom of the domain interior ;
   // - myndof intrf : integer equal to the degree of freedom of the domain interface ;
   // - lenindintrf : integer equal to the size of the array myindexintrf
   // - mysizeintrf : integer equal to the size of the domain interface ;
   // - gballintrf : integer equal to the total size of each interfaces in each processus ;
    - mynbvi : integer equal to the number of neighbor of the processus ;
    - myindexVi(:) : list of each neighbors of the domain by rank processus ;
    - myptrindexVi(:) : pointer to the first node of the interface of each domain ;
    - myindexintrf (:) : list of the node of the domain interface, index are in local ordering ;
    - myinterface(:) : conversion from local ordering to global ordering ;
    - myndoflogicintrf : Integer corresponding to the number of node in the interface wich who the domain is responsable for ;
   // - mylogicintrf (:) : list of the nodes that this domain is responsible for (in the local ordering).
  */
    DMPH_distributed_interface_c(&id, myndof, myndofintrf, myinterface, mynbvi, myindexvi, myptrindexvi, myindexintrf);
}



// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

maphysFEMSparseSolver::maphysFEMSparseSolver(void) : dtkSparseSolver<double>(), d(new maphysFEMSparseSolverPrivate)
{
    d->matrix = nullptr;
    d->rhs_vector = nullptr;
    d->sol_vector = nullptr;
    d->mpi_rank = -1;

    d->number_of_cores=0;
    d->number_of_nodes=0;

    d->residual_reduction_order = 1.e-5;
    d->number_of_iterations = 100;
    d->reuse_facto = false;

    d->A_initialized = false;
    d->rhs_initialized = false;
    d->sol_initialized = false;

    d->rows = nullptr;
    d->cols = nullptr;
    d->rhs_values = nullptr;
    d->sol_values = nullptr;
    d->nnz = 0;
    d->nb_itrf_with_lower_proc =0;

    d->id.job=MAPHYS_INIT;

    MPI_Comm *comm = static_cast<MPI_Comm *>(dtkDistributed::communicator::instance()->data());
    d->id.comm = (*comm);

    dmph_maphys_driver_c(&d->id);
}

maphysFEMSparseSolver::~maphysFEMSparseSolver(void)
{
    d->id.job=MAPHYS_END;
    dmph_maphys_driver_c(&d->id);
    if(d->rows) delete [] d->rows;
    if(d->cols) delete [] d->cols;
    if(d->rhs_values) delete [] d->rhs_values;
    if(d->sol_values) delete [] d->sol_values;

//    if(d->myindexvi) delete [] d->myindexvi;
//    if(d->myptrindexvi) delete [] d->myptrindexvi;
//    if(d->myindexintrf) delete [] d->myindexintrf;
//    if(d->myinterface) delete [] d->myinterface;

    delete d;
}


//first set the matrix sequentially
void maphysFEMSparseSolver::setMatrix(dtkSparseMatrix<double> *matrix)
{
    d->matrix = matrix;

    if (!d->A_initialized) {
        d->initDistributedInterface();

        //get the DDData
        const dtkDistributedGraphTopology::DDData& dd = matrix->graph()->m_dd;
        d->nnz = dd.local_edge_to_vertex.size();
        d->rows = new MAPHYS_INT[d->nnz];
        d->cols = new MAPHYS_INT[d->nnz];
        qlonglong count = 0;

        d->nb_local_rows= dd.local_vertex_to_edge.size() - 1;
        for (qlonglong i = 0; i < dd.local_vertex_to_edge.size() - 1; ++i) {
             qlonglong pos = dd.local_vertex_to_edge.at(i);
             qlonglong end = dd.local_vertex_to_edge.at(i+1);

             for(; pos < end; ++pos) {
                 d->rows[count] = i + 1;
                 d->cols[count++] = dd.glob_to_loc[dd.local_edge_to_vertex.at(pos)] + 1;
             }
         }
         auto it  = dd.map_remote.cbegin();
         auto end = dd.map_remote.cend();
         for (; it != end; ++it) {
             //if(!(*it).empty())
                 //qDebug() << d->mpi_rank << "add remote cols" << *it;
             for (qlonglong c_id : *it) {
                 d->rows[count]   = dd.glob_to_loc[it.key()] + 1;
                 d->cols[count++] = dd.glob_to_loc[c_id] + 1;
             }
         }
    }

     //set Maphys matrix
     d->id.sym = 0; // 0:nonsym 1:SDP 2:sym
     d->id.n = d->nb_local_rows;
     d->id.nnz = d->nnz;
     d->id.rows = d->rows;
     d->id.cols = d->cols;
     d->id.values = d->matrix->data();

     d->A_initialized = true;

     // QString filename = "maphys_matrix_" %  QString("%1").arg(d->mpi_rank,4,10,QChar('0')) % ".txt";
     // QFile file(filename);
     // if (file.open(QIODevice::ReadWrite)) {
     // 	 QTextStream stream(&file);
     // 	 stream << " %MatrixMarket matrix coordinate real general " << endl;
     // 	 stream <<  d->nb_local_rows << " " <<  d->nb_local_rows << " " << d->nnz << endl;
     // 	 for(int i=0; i< d->nnz; ++i)
     // 	     stream << d->rows[i] << " " << d->cols[i] << " " << d->id.values[i] << endl;
     // }

}

//matrix has to be set before
void maphysFEMSparseSolver::setRHSVector(dtkVector<double> *rhs_vector)
{
    //rhs has to be of the size d->matrix->row_count
    d->rhs_vector = rhs_vector;
    if(!d->rhs_initialized) {
        d->rhs_values = new double[d->nb_local_rows];
    }

    auto it = d->rhs_vector->begin();
    auto ite = d->rhs_vector->end();
    //qDebug() << d->mpi_rank << it.id() << ite.id() << d->nb_local_rows;
    qlonglong i=0;

    //first fill interior values
    for(; i < d->myndofinterior; ++it, ++i) {
        d->rhs_values[i] = *it;
    }

    //then fill with 0 for the interfaces with ,lower rank
    qlonglong end = d->myndofinterior + d->myndofintrf - d->myndoflogicintrf;
    for(; i < end; ++i)
        d->rhs_values[i] = 0.0;

    //now fill values for interfaces belonging to me
    for(; it != ite; ++it, ++i) {
        d->rhs_values[i] = *it;
    }

    if(i != d->nb_local_rows)
        qDebug() << "error filling the rhs, lasti=" << i << " and nb_local_rows = " << d->nb_local_rows;

    d->id.rhs =  d->rhs_values;
    d->rhs_initialized = true;

    // QString filename = "maphys_rhs_" %  QString("%1").arg(d->mpi_rank,4,10,QChar('0')) % ".txt";
    // QFile file(filename);
    // if (file.open(QIODevice::ReadWrite)) {
    // 	QTextStream stream(&file);
    // 	stream << " %MatrixMarket rhs coordinate real general " << endl;
    // 	stream <<  d->nb_local_rows << " " <<  1  << " "<< d->nb_local_rows << endl;
    // 	for(int i=0; i< d->nb_local_rows; ++i)
    // 	    stream << d->rhs_values[i] << endl;
    // }

}

void maphysFEMSparseSolver::setSolutionVector(dtkVector<double> *sol_vector)
{
    d->sol_vector = sol_vector;

    if(!d->sol_initialized) {
        d->sol_values = new double[d->nb_local_rows];
      }

    auto it = d->sol_vector->begin();
    auto ite = d->sol_vector->end();
    qlonglong i=0;

    /*  maphys doesn't use this hint....

    //first fill interior values
    for(; i < d->myndofinterior; ++it, ++i) {
        d->sol_values[i] = *it;
    }

    //then fill with 0 for the interfaces with ,lower rank
    qlonglong end = d->myndofinterior + d->myndofintrf - d->myndoflogicintrf;
    for(; i < end; ++i)
        d->sol_values[i] = 0.0;

    //now fill values for interfaces belonging to me
    for(; it != ite; ++it, ++i) {
        d->sol_values[i] = *it;
    }

    */

    d->id.sol = d->sol_values;
    d->sol_initialized = true;
}

void maphysFEMSparseSolver::setOptionalParameters(const QHash<QString, int>& parameters)
{
    {
     QHash<QString, int>::const_iterator it = parameters.find("number_of_iterations");
     if(it != parameters.end())
         d->number_of_iterations = it.value();
    }

    {
     QHash<QString, int>::const_iterator it = parameters.find("reuse_facto");
     if(it != parameters.end())
         d->reuse_facto = (it.value() != 0);
    }
}

void maphysFEMSparseSolver::setOptionalParameters(const QHash<QString, double>& parameters)
{
     QHash<QString, double>::const_iterator it = parameters.find("residual_reduction_order");
     if(it != parameters.end())
         d->residual_reduction_order = it.value();
}


void maphysFEMSparseSolver::run(void)
{
    //check if we passed a initial solution. If no, create a dummy first solution
    if (!d->sol_initialized) {
        this->setSolutionVector(new dtkVector<double>(*(d->rhs_vector)));
    }
    
   // options
   // verbosity of maphys : 0 no print, 1 print errors, 2 print eeres+warnings, .. 6
    d->id.ICNTL(4) = 0 ;
   // when to print list of controls : 0 never print, 1 once at the begining, 2 at each step
    d->id.ICNTL(5) = 0;
    //when to print list of info
    d->id.ICNTL(6) = 0;
   // ICNTL(13): choice of sparse direct sover: 1:Mumps or 2: Pastix
   d->id.ICNTL(13) = 2;
   //ICNTL(20): 1: GMRES, 2: CG, 3:automatic (CG if matrix is SDP, GMRES otherwise) with 3 as default
   d->id.ICNTL(20) = 3;
   // ICNTL(21): control the type of preconditioner: 1:dense 2:sparse(default) LOOK CONSTRAINTS IN DOC
   d->id.ICNTL(21) = 2;

   // ICNTL(23) controls the iterative solver. Controls whether the user wishes to supply
   // an initial guess of solution vector in (d->id.SOL).
   // 0 = No initilization guess (start from null vector)
   // 1 = User defined initialization guess
   d->id.ICNTL(23) = 0;


   // ICNTL(24) controls the iterative solver. It define the maximumn number of iterations. 0, maphys will performs at most 100 iterations, >0 maphys will perform ICNTL(24) iterations
   d->id.ICNTL(24) = d->number_of_iterations;

   //ICNTL(26) restart parameter of GMRES . number of iteratios between each restart
   d->id.ICNTL(26) = d->number_of_iterations;


   //ICNTL(27) ontrols the iterative solver, it is the method used to perform multiplication between the Schur complement matrix and a vector. It should be set before the solving step on each process. It can be set on each process before the preconditioning step to allow possible memory footprint reduction
   d->id.ICNTL(27)=0;

   //ICNTL(36) Is the control paramater to set thread binding : 0 = Do not bind ;1 = Thread to core binding ; 2 = Grouped bind.  -1 = do nothing
   d->id.ICNTL(36)=1;

   //ICNTL(37) the number of nodes
   d->id.ICNTL(37) = d->getNumberOfNodes();

   //ICNTL(38) the number of cores on each nodes
   d->id.ICNTL(38) = d->getNumberOfCores();

   //ICNTL(39) the number of the number of threads per domains
   d->id.ICNTL(39) = omp_get_max_threads();

   //ICNTL(40) the number of domains
   d->id.ICNTL(40) = d->mpi_size;

   //Is the control paramater that active the 2 level of parallelism version. 0 = only MPI; 1 = MPI+OpenMP
   d->id.ICNTL(42)=1;

   //   dtkInfo() << "maphys openmp : " << d->id.ICNTL(37) << " " <<  d->id.ICNTL(38) << d->id.ICNTL(39) << d->id.ICNTL(40);


   //ICNTL(43) specifies how the input system is set : 1 centralized on master, 2 distributed
   d->id.ICNTL(43) = 2;

   //RCNTL(11): sparsifying level (in the sparse case) with 10^-4 as default
   d->id.RCNTL(11) = 0.0001; // TODO with sparse and with dense

   //RCNTL(21): tolerance for the solution during the iterative Krylov process with 10^-5 as default
   d->id.RCNTL(21) = d->residual_reduction_order;

   //solve
   if(d->reuse_facto) {
       //d->id.job = 3; // do precond
       //dmph_maphys_driver_c(&d->id);
       d->id.job = 4; // just do a solve, no facto, no precond
   }
   else {
       d->id.job = MAPHYS_SOLVE;
   }
   dmph_maphys_driver_c(&d->id);

   if(d->mpi_rank==0) {
       //get informations on execution
       if(d->id.IINFO(1) < 0) qDebug() << "MAPHYS Failure  IINFO(1)=" << d->id.IINFO(1);
       if(d->id.IINFO(1) > 0) qDebug() << "MAPHYS Warning  IINFO(1)=" << d->id.IINFO(1);
       if(d->id.IINFO(1) == 0) {
           qDebug() << "MAPHYS Success !!!";
           qDebug() << "conversion time to local system : " << d->id.RINFO(9) << "s";
           qDebug() << "analysis      step : " << d->id.RINFO(4) << "s";
           qDebug() << "factorisation step : " << d->id.RINFO(5) << "s";
           qDebug() << "precond       step : " << d->id.RINFO(6) << "s";
           qDebug() << "solution      step : " << d->id.RINFO(7) << "s";


           int num_iterations = d->id.IINFOG(5);
           double final_res_norm = d->id.RINFOG(4);

           qDebug() << "maphys finish in" << num_iterations <<"iterations with a final res norm of"
                    << final_res_norm;
       }
   }

}


dtkVector<double>* maphysFEMSparseSolver::solutionVector(void) const
{

    qlonglong offset = d->sol_vector->begin().id();
    qlonglong i=0;

    //first fill interior values
    for(; i < d->myndofinterior; ++i) {
        d->sol_vector->setAt(i+offset, d->sol_values[i]);
    }

    //now fill values for interfaces belonging to me
    i += offset;
    qlonglong offset_values =  d->myndofintrf - d->myndoflogicintrf -offset; // values to ignore = number of interfaces with lower rank
    for(; i<d->sol_vector->end().id(); ++i)
        d->sol_vector->setAt(i, d->sol_values[i+offset_values]);

    return d->sol_vector;
}

const dtkArray<double>& maphysFEMSparseSolver::stats(void) const
{
    return d->stats;
}
//
// maphysFEMSparseSolver.cpp ends here
