// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

class maphysFEMSparseSolverPrivate;

class maphysFEMSparseSolver : public dtkSparseSolver<double>
{
public:
     maphysFEMSparseSolver(void);
    ~maphysFEMSparseSolver(void);

public:
    void setMatrix(dtkSparseMatrix<double>* matrix);
    void setRHSVector(dtkVector<double>* rhs_vector);
    void setSolutionVector(dtkVector<double>* sol_vector);

    void setOptionalParameters(const QHash<QString, double>& parameters);
    void setOptionalParameters(const QHash<QString, int>& parameters);

public:
    void run(void);

public:
    dtkVector<double>* solutionVector(void) const;
    const dtkArray<double>& stats(void) const;

protected:
    maphysFEMSparseSolverPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkSparseSolver<double>* maphysFEMSparseSolverCreator(void)
{
    return new maphysFEMSparseSolver();
}

// 
// maphysFEMSparseSolver.h ends here
