// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:


#pragma once

#include <dtkCore>

#include <dtkSparseSolver.h>

class maphysFEMSparseSolverPlugin : public dtkSparseSolverPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkSparseSolverPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.maphysFEMSparseSolverPlugin" FILE "maphysFEMSparseSolverPlugin.json")

public:
     maphysFEMSparseSolverPlugin(void) {}
    ~maphysFEMSparseSolverPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

// 
// maphysFEMSparseSolverPlugin.h ends here
