// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "maphysFEMSparseSolver.h"
#include "maphysFEMSparseSolverPlugin.h"

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

// ///////////////////////////////////////////////////////////////////
// maphysFEMSparseSolverPlugin
// ///////////////////////////////////////////////////////////////////

void maphysFEMSparseSolverPlugin::initialize(void)
{
    dtkLinearAlgebraSparse::solver::pluginFactory<double>().record("maphysFEMSparseSolver", maphysFEMSparseSolverCreator);
}

void maphysFEMSparseSolverPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(maphysFEMSparseSolver)

// 
// maphysFEMSparseSolverPlugin.cpp ends here
