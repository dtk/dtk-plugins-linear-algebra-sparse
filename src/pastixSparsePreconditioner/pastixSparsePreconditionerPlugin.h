// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:


#pragma once

#include <dtkCore>

#include <dtkSparsePreconditioner.h>

class pastixSparsePreconditionerPlugin : public dtkSparsePreconditionerPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkSparsePreconditionerPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.pastixSparsePreconditionerPlugin" FILE "pastixSparsePreconditionerPlugin.json")

public:
     pastixSparsePreconditionerPlugin(void) {}
    ~pastixSparsePreconditionerPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

// 
// pastixSparsePreconditionerPlugin.h ends here
