// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:


#undef PLUGIN_HAVE_MPI
#ifdef PLUGIN_HAVE_MPI
#  include <mpi.h>
#else
#  define MPI_Comm int;
#  define MPI_COMM_WORLD 0
#endif

#include <complex>
namespace PaStiX {
extern "C" {
#include <pastix.h>
}
}

#include "../pastixSparseSolverDirect/pastixSparseSolverDirect.h"
#include "pastixSparsePreconditioner.h"

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

class pastixSparsePreconditionerPrivate
{
public:
    dtkSparseMatrix<double> *matrix;
    pastixSparseSolverDirect *solver;

public:
    PaStiX::pastix_data_t * pastix_data;
    MPI_Comm                pastix_comm;
    PaStiX::pastix_int_t    ncol;               /* Size of the matrix  */
    PaStiX::pastix_int_t   *colptr; /* Indexes of first element of each
                         column in row and
                         values */
    PaStiX::pastix_int_t   *rows; /* Row of each element of the
                         matrix */
    double *values; /* Value of each element
                         of the matrix */
    PaStiX::pastix_int_t   *iparm;  /* integer parameters for pastix */
    double                 *dparm;  /* floating parameters for pastix */
    PaStiX::pastix_int_t   *perm; /* Permutation
                   tabular */
    PaStiX::pastix_int_t   *invp; /* Reverse permutation
                   tabular */

    bool is_first_iter;

};

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

pastixSparsePreconditioner::pastixSparsePreconditioner(void) : dtkSparsePreconditioner<double>(), d(new pastixSparsePreconditionerPrivate)
{
    d->pastix_data = NULL;
    d->pastix_comm = MPI_COMM_WORLD;
    d->colptr	 = NULL;
    d->rows	 = NULL;
    d->values	 = NULL;
    d->iparm	 = new PaStiX::pastix_int_t[PaStiX::IPARM_SIZE];
    d->dparm	 = new double[PaStiX::DPARM_SIZE];
    d->perm	 = NULL;
    d->invp	 = NULL;
    d->is_first_iter = false;
}

pastixSparsePreconditioner::~pastixSparsePreconditioner(void)
{
    d->matrix     = NULL;

    if (d->colptr) delete[] d->colptr;
    if (d->rows)   delete[] d->rows;
    if (d->values) d->values = NULL;
    if (d->iparm ) delete[] d->iparm;
    if (d->dparm ) delete[] d->dparm;
    if (d->perm  ) delete[] d->perm;
    if (d->invp  ) delete[] d->invp;

}

void pastixSparsePreconditioner::setMatrix(const dtkSparseMatrix<double>& matrix)
{
    if (!d->values) {
        d->matrix = &const_cast<dtkSparseMatrix<double>&>(matrix);
        d->ncol   = d->matrix->colCount();
        d->colptr = new PaStiX::pastix_int_t[d->ncol+1];
        d->rows   = new PaStiX::pastix_int_t[d->matrix->nonZeroCount()];
        d->perm   = new PaStiX::pastix_int_t[d->ncol];
        d->invp   = new PaStiX::pastix_int_t[d->ncol];
        d->values = d->matrix->rawData();
        d->is_first_iter = true;
    }
    int iter = 0;
    d->colptr[0] = 1;
    for (qlonglong i = 0; i < d->matrix->rowCount(); ++i) {
        dtkSparseMatrix<double>::const_iterator cit  = d->matrix->cbegin(i);
        dtkSparseMatrix<double>::const_iterator cend = d->matrix->cend(i);
        d->colptr[i+1] = d->colptr[i] + (cend-cit);
        for(;cit != cend; ++cit) {
            qlonglong j = cit.coordinates().second + 1;
            d->rows[iter++]     = j;
        }
    }
}

void pastixSparsePreconditioner::setOptionalParameters(const dtkArray<double>& parameters)
{
}

int pastixSparsePreconditioner::run(void)
{
#ifdef PLUGIN_HAVE_MPI
    // -- Init MPI
    int required = MPI_THREAD_MULTIPLE;
    int provided = -1;
    int initialized = 0;

    MPI_Initialized(&initialized);
    if (!initialized)
        MPI_Init_thread(NULL, NULL, required, &provided);
    int mpi_id; MPI_Comm_rank(MPI_COMM_WORLD, &mpi_id);
#else

#endif
    /* Init parameters */
    if (d->is_first_iter) {
        d->iparm[PaStiX::IPARM_MODIFY_PARAMETER] = PaStiX::API_NO;
        d->iparm[PaStiX::IPARM_VERBOSE]          = PaStiX::API_VERBOSE_NOT;
        PaStiX::d_pastix(&(d->pastix_data),
                         d->pastix_comm,
                         d->ncol,
                         d->colptr,
                         d->rows,
                         d->values,
                         d->perm,
                         d->invp,
                         NULL,
                         1,
                         d->iparm,
                         d->dparm);
        d->is_first_iter = false;
    }

    /* Preprocess, Fact */
    d->iparm[PaStiX::IPARM_TRANSPOSE_SOLVE]  = PaStiX::API_YES;
    d->iparm[PaStiX::IPARM_VERBOSE]          = PaStiX::API_VERBOSE_NOT;
    d->iparm[PaStiX::IPARM_SYM]              = PaStiX::API_SYM_NO;
    d->iparm[PaStiX::IPARM_START_TASK]       = PaStiX::API_TASK_ORDERING;
    d->iparm[PaStiX::IPARM_FACTORIZATION]    = PaStiX::API_FACT_LU;
    d->iparm[PaStiX::IPARM_END_TASK]         = PaStiX::API_TASK_NUMFACT;
    PaStiX::d_pastix(&(d->pastix_data),
                     d->pastix_comm,
                     d->ncol,
                     d->colptr,
                     d->rows,
                     d->values,
                     d->perm,
                     d->invp,
                     NULL,
                     1,
                     d->iparm,
                     d->dparm);

    return 1;
}

PaStiX::pastix_data_t *pastixSparsePreconditioner::pastixData(void) const
{
  return d->pastix_data;
}
// 
// pastixSparsePreconditioner.cpp ends here
