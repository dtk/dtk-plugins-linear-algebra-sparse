// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "pastixSparsePreconditioner.h"
#include "pastixSparsePreconditionerPlugin.h"

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

dtkSparsePreconditioner<double>* pastixSparsePreconditionerCreator(void);

// ///////////////////////////////////////////////////////////////////
// pastixSparsePreconditionerPlugin
// ///////////////////////////////////////////////////////////////////

void pastixSparsePreconditionerPlugin::initialize(void)
{
    dtkLinearAlgebraSparse::preconditioner::pluginFactory<double>().record("pastixSparsePreconditioner", pastixSparsePreconditionerCreator);
}

void pastixSparsePreconditionerPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(pastixSparsePreconditioner)

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

dtkSparsePreconditioner<double>* pastixSparsePreconditionerCreator(void)
{
    return new pastixSparsePreconditioner();
}

// 
// pastixSparsePreconditionerPlugin.cpp ends here
