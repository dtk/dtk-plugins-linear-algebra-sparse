// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

class pastixSparsePreconditionerPrivate;

namespace PaStiX {
  extern "C" {
struct pastix_data_t;
  }
}

class pastixSparsePreconditioner : public dtkSparsePreconditioner<double>
{
    Q_OBJECT

public:
     pastixSparsePreconditioner(void);
    ~pastixSparsePreconditioner(void);

public:
    void setMatrix(const dtkSparseMatrix<double>& matrix);

    void setOptionalParameters(const dtkArray<double>& parameters);

public:
    int run(void);

public:
    dtkSparseSolver<double>& solver(void) const;

public:
    PaStiX::pastix_data_t *pastixData(void) const;

protected:
    pastixSparsePreconditionerPrivate *d;
};

// 
// pastixSparsePreconditioner.h ends here
