// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "maphysSparseSolver.h"

#include <mpi.h>
#include <dtkDistributed>

extern "C" {
#include "dmph_maphys_type_c.h"
}

// tristan interface

// extern "C" void DMPH_maphys_driver_tristan(int job, int comm, int sym, int n, int nnz, 
//                                            int *rows, int *cols, double *values, int nrhs, 
//                                            double *rhs, double *sol, int *icntl, double *rcntl,
//                                            int *iinfo, double *rinfo, int *iinfog, double *rinfog,
//                                            int *iinfomin, int *iinfomax, int *iinfoavg, 
//                                            int *iinfosig, double *rrinfomin, double *rrinfomax, 
//                                            double *rrinfoavg, double *rinfosig);


// void DMPH_maphys_driver_c_tristan(DMPH_maphys_t_c *mphs_c)
// {
//     //convert mpi communicator

//     //if init
//     if(mphs_c->job == -1) {
//         mphs_c->rows = NULL;
//         mphs_c->cols = NULL;
//         mphs_c->values = NULL;
//         mphs_c->rhs = NULL;
//         mphs_c->sol = NULL;
//         mphs_c->n=0;
//         mphs_c->nnz=0;
//         mphs_c->nrhs=0;
//     }

//     DMPH_maphys_driver_tristan(mphs_c->job, mphs_c->fcomm, mphs_c->sym, mphs_c->n, mphs_c->nnz, 
//                                mphs_c->rows, mphs_c->cols, mphs_c->values, mphs_c->nrhs, 
//                                mphs_c->rhs, mphs_c->sol, mphs_c->icntl, mphs_c->rcntl,
//                                mphs_c->iinfo, mphs_c->rinfo, mphs_c->iinfog, mphs_c->rinfog,
//                                mphs_c->iinfomin, mphs_c->iinfomax, mphs_c->iinfoavg, 
//                                mphs_c->iinfosig, mphs_c->rinfomin, mphs_c->rinfomax, 
//                                mphs_c->rinfoavg, mphs_c->rinfosig);
// }


// end tristan interface

#define MAPHYS_INIT -1
#define MAPHYS_END -2
#define MAPHYS_SOLVE 6
#define USE_COMM_WORLD -987654
#define ICNTL(I) icntl[(I)-1] /* macro s.t. indices match documentation */
#define RCNTL(I) rcntl[(I)-1] /* macro s.t. indices match documentation */
#define IINFO(I) iinfo[(I)-1] /* macro s.t. indices match documentation */
#define RINFO(I) rinfo[(I)-1] /* macro s.t. indices match documentation */
#define RINFOG(I) rinfog[(I)-1] /* macro s.t. indices match documentation */
#define IINFOG(I) iinfog[(I)-1] /* macro s.t. indices match documentation */
// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

#define MAPHYS_INT int

class maphysSparseSolverPrivate
{
public:
    dtkSparseMatrix<double> *matrix;
    dtkVector<double> *rhs_vector;
    dtkVector<double> *sol_vector;

    QHash<QString, int> params_int;
    QHash<QString, double> params_real;

    int number_of_iterations;
    double residual_reduction_order;

public:
    qlonglong my_rank; // mpi_rank
    dtkArray<double> stats;

public:
    bool A_initialized;
    bool rhs_initialized;
    bool sol_initialized;

public:
    int *rows;
    int *cols;
    double *mat_values;
    int nnz;

public:
    double *rhs_values;
    double *sol_values;
    

public:
    DMPH_maphys_t_c id;



private:
    MAPHYS_INT myndof;
    MAPHYS_INT myndofinterior;
    MAPHYS_INT myndofintrf;
    MAPHYS_INT mysizeintrf;
    MAPHYS_INT gballintrf;
    MAPHYS_INT mynbci;
    QList<MAPHYS_INT> myindevvi_list;
    QList<MAPHYS_INT> myptrindexvi_list;
    QList<MAPHYS_INT> myindexintrf_list;
    QList<MAPHYS_INT> myinterface_list;
    QList<MAPHYS_INT> myndoflogicintrf_list;
    QList<MAPHYS_INT> mylogicintrf_list;
    MAPHYS_INT *myindevvi;
    MAPHYS_INT *myptrindexvi;
    MAPHYS_INT *myindexintrf;
    MAPHYS_INT *myinterface;
    MAPHYS_INT *myndoflogicintrf;
    MAPHYS_INT *mylogicintrf;

};



// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

maphysSparseSolver::maphysSparseSolver(void) : dtkSparseSolver<double>(), d(new maphysSparseSolverPrivate)
{
    d->matrix = NULL;
    d->rhs_vector = NULL;
    d->sol_vector = NULL;
    d->my_rank = -1;

    d->residual_reduction_order = 1.e-5;
    d->number_of_iterations = 100;

    d->A_initialized = false;
    d->rhs_initialized = false;
    d->sol_initialized = false;

    d->rows = NULL;
    d->cols = NULL;
    d->mat_values = NULL;
    d->rhs_values = NULL;
    d->sol_values = NULL;
    d->nnz = 0;

    d->id.job=MAPHYS_INIT;

    MPI_Comm *comm = static_cast<MPI_Comm *>(dtkDistributed::communicator::instance()->data());
    d->id.comm = (*comm);

    dmph_maphys_driver_c(&d->id);
}

maphysSparseSolver::~maphysSparseSolver(void)
{
    d->id.job=MAPHYS_END;
    dmph_maphys_driver_c(&d->id);
  
    delete[] d->rows;
    delete[] d->cols;
    delete[] d->mat_values;
    delete[] d->sol_values;
    delete[] d->rhs_values;

    delete d;
}


//first set the matrix sequentially
void maphysSparseSolver::setMatrix(dtkSparseMatrix<double> *matrix)
{
    d->matrix = matrix;

    //only process 0 is setting the matrix
    d->my_rank =matrix->graph()->wid(); 
    
    if(d->my_rank == 0)
    {
        if (!d->A_initialized) {
            qlonglong count=0;
            d->nnz = d->matrix->nonZeroCount();
            d->rows  = new MAPHYS_INT[d->nnz];
            d->cols = new MAPHYS_INT[d->nnz];
            d->mat_values = new double[d->nnz];

            for(qlonglong i=0; i<d->matrix->rowCount(); ++i)
            {
                dtkSparseMatrix<double>::line_iterator lit = d->matrix->lineAt(i);
                dtkSparseMatrixLineElement<double>  it = lit.begin();
                dtkSparseMatrixLineElement<double>  end = lit.end();
                for (; it != end; it++) {
                    qlonglong j = it.id();
                    //add each element
                    d->rows[count] = static_cast<MAPHYS_INT>(i)+1;
                    d->cols[count] = static_cast<MAPHYS_INT>(j)+1;
                    d->mat_values[count] = *it;
                    count++;
                }
            }

            if(count != d->nnz) {
                qDebug() << "WARNING, count != d->nnz, count=" << count 
                         << "d->nnz=" << d->nnz;
            }
        }
        
//        for(int i=0; i<d->nnz; ++i) {
//            d->mat_values[i] = d->matrix->at(d->rows[i], d->cols[i]);
//            if(d->rows[i]==1000) {
//                qDebug() << d->cols[i] << d->mat_values[i];
//                d->mat_values[i] = i*1.0;
//            }
        //       }

        //set Maphys matrix
        d->id.sym = 0; // 0:nonsym 1:SDP 2:sym 
        d->id.n = d->matrix->rowCount();
        d->id.nnz = d->nnz;       
        d->id.rows = d->rows;
        d->id.cols = d->cols;
        d->id.values = d->mat_values;
        
    } // end if rank == 0

    d->A_initialized = true;

}

//matrix has to be set before
void maphysSparseSolver::setRHSVector(dtkVector<double> *rhs_vector)
{
    //rhs has to be of the size d->matrix->row_count
    d->rhs_vector = rhs_vector;
    if(d->my_rank==0) {
        if(!d->rhs_initialized)
            d->rhs_values = new double[d->matrix->rowCount()];

        if(d->matrix->rowCount() != d->rhs_vector->size())
            qDebug() << "error matrix_row_count=" << d->matrix->rowCount() 
                     << "and d->rhs_vector->size()=" << d->rhs_vector->size();

        for(qlonglong i=0; i< d->rhs_vector->size(); ++i)
            d->rhs_values[i] = d->rhs_vector->at(i);

        d->id.rhs = d->rhs_values;        
    }

    d->rhs_initialized = true;

}

void maphysSparseSolver::setSolutionVector(dtkVector<double> *sol_vector)
{
    d->sol_vector = sol_vector;

    if(d->my_rank==0) {
        if(!d->sol_initialized)
            d->sol_values = new double[d->matrix->rowCount()];

        if(d->matrix->rowCount() != d->sol_vector->size())
            qDebug() << "error matrix_row_count=" << d->matrix->rowCount() 
                     << "and d->sol_vector->size()=" << d->sol_vector->size();

        for(qlonglong i=0; i< d->sol_vector->size(); ++i)
            d->sol_values[i] = d->sol_vector->at(i);

        d->id.sol = d->sol_values;        
    }


    d->sol_initialized = true;
//    d->id.sol = d->sol_vector->data();
}

void maphysSparseSolver::setOptionalParameters(const QHash<QString, int>& parameters)
{
     QHash<QString, int>::const_iterator it = parameters.find("number_of_iterations");

     if(it != parameters.end())
         d->params_int.insert("number_of_iterations", it.value());

     d->number_of_iterations = d->params_int.value("number_of_iterations");
}

void maphysSparseSolver::setOptionalParameters(const QHash<QString, double>& parameters)
{
     QHash<QString, double>::const_iterator it = parameters.find("residual_reduction_order");

     if(it != parameters.end())
         d->params_real.insert("residual_reduction_order", it.value());

     d->residual_reduction_order = d->params_real.value("residual_reduction_order");
}


void maphysSparseSolver::run(void)
{
/*    int required = MPI_THREAD_MULTIPLE;
    int provided = -1;
    int initialized = 0;

    MPI_Initialized(&initialized);
    if (!initialized) {
        MPI_Init_thread(NULL, NULL, required, &provided);
        qDebug() << "mpi_init_threas in maphysSparseSolver.cpp";
    }
*/
    //check if we passed a initial solution. If no, create a dummy first solution
    if (!d->sol_initialized) {
        this->setSolutionVector(new dtkVector<double>(*(d->rhs_vector)));
    }

    //wait for rank 0 to set the matrix
    d->matrix->graph()->communicator()->barrier();


   // options
   // verbosity of maphys : 0 no print, 1 print errors, 2 print eeres+warnings, .. 6
   d->id.ICNTL(4) = 5 ;
   // when to print list of controls : 0 never print, 1 once at the begining, 2 at each step
   d->id.ICNTL(5) = 0;
    //when to print list of info
   d->id.ICNTL(6) = 2;
   // ICNTL(13): choice of sparse direct sover: 1:Mumps or 2: Pastix
   d->id.ICNTL(13) = 2;
   //ICNTL(20): 1: GMRES, 2: CG, 3:automatic (CG if matrix is SDP, GMRES otherwise) with 3 as default
   d->id.ICNTL(20) = 1;
   // ICNTL(21): control the type of preconditioner: 1:dense 2:sparse(default)
   d->id.ICNTL(21) = 2; //1
   
    // ICNTL(23) controls the iterative solver. Controls whether the user wishes to supply 
    //           an initial guess of solution vector in (d->id.SOL). 0 = No initilization guess 
    //          (start from null vector) , 1 = User defined initialization guess
   d->id.ICNTL(23) = 1;

   // ICNTL(24) controls the iterative solver. It define the maximumn number of iterations. 0, maphys will performs at most 100 iterations, >0 maphys will perform ICNTL(24) iterations
   d->id.ICNTL(24) = d->number_of_iterations;

   //ICNTL(27) ontrols the iterative solver, it is the method used to perform multiplication between the Schur complement matrix and a vector. It should be set before the solving step on each process. It can be set on each process before the preconditioning step to allow possible memory footprint reduction
   d->id.ICNTL(27)=1;

   //ICNTL(36) Is the control paramater that active the 2 level of parallelism version,
   
   //ICNTL(43) specifies how the input system is set : 1 centralized on master, 2 distributed
   // TODO TODO TODO PARALLEL VERSION
   d->id.ICNTL(43) = 1;


   //RCNTL(11): sparsifying level (in the sparse case) with 10^-4 as default
   //RCNTL(21): tolerance for the solution during the iterative Krylov process with 10^-5 as default
   d->id.RCNTL(21) = d->residual_reduction_order;
   
   //write matrix in matrix market format
   //d->id.WRITE_MATRIX= "maphys_matrix";




   //solve
   d->id.job = MAPHYS_SOLVE;
   qDebug() << "MAPHYS_SOLVE launch";
   dmph_maphys_driver_c(&d->id);
   qDebug() << "MAPHYS_SOLVE Ok";

   if(d->my_rank==0) {
       //get informations on execution
       if(d->id.IINFO(1) < 0) qDebug() << "MAPHYS Failure  IINFO(1)=" << d->id.IINFO(1);
       if(d->id.IINFO(1) > 0) qDebug() << "MAPHYS Warning  IINFO(1)=" << d->id.IINFO(1);
       if(d->id.IINFO(1) == 0) {
           qDebug() << "MAPHYS Success !!!";
           qDebug() << "conversion time to local system : " << d->id.RINFO(9) << "s";
           qDebug() << "analysis      step : " << d->id.RINFO(4) << "s";
           qDebug() << "factorisation step : " << d->id.RINFO(5) << "s";
           qDebug() << "precond       step : " << d->id.RINFO(6) << "s";
           qDebug() << "solution      step : " << d->id.RINFO(7) << "s";
           
           
           int num_iterations = d->id.IINFOG(5);
           double final_res_norm = d->id.RINFOG(4);
           
           qDebug() << "maphys finish in" << num_iterations <<"iterations with a final res norm of" 
                    << final_res_norm;
       }
   }
   this->solutionVector();
        
}


dtkVector<double>* maphysSparseSolver::solutionVector(void) const
{
    
    if(d->my_rank==0) {
        for(qlonglong i=0; i< d->sol_vector->size(); ++i) {   
            d->sol_vector->setAt(i, d->sol_values[i]);
        }
        qDebug() << " copy complete";
    }
    d->matrix->graph()->communicator()->barrier();


    return d->sol_vector;
}

const dtkArray<double>& maphysSparseSolver::stats(void) const
{
    return d->stats;
}
// 
// maphysSparseSolver.cpp ends here
