// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:


#pragma once

#include <dtkCore>

#include <dtkSparseSolver.h>

class maphysSparseSolverPlugin : public dtkSparseSolverPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkSparseSolverPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.maphysSparseSolverPlugin" FILE "maphysSparseSolverPlugin.json")

public:
     maphysSparseSolverPlugin(void) {}
    ~maphysSparseSolverPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

// 
// maphysSparseSolverPlugin.h ends here
