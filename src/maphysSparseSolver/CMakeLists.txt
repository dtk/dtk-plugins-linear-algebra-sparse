## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

project(maphysSparseSolverPlugin C CXX Fortran)

## ###################################################################
## Resolve dependencies
## ###################################################################

find_package(maphys)

if(MAPHYS_FOUND)

  set(CMAKE_Fortran_MODULE_DIRECTORY  ${MAPHYS_MODULES_DIR})

  include_directories(${MPI_Fortran_INCLUDE_PATH})
  set(COMPILE_FLAGS ${COMPILE_FLAGS} ${MPI_CXX_COMPILE_FLAGS})
  add_definitions(-D_MPI)

  find_package(OpenMP REQUIRED)
  find_package(hwloc REQUIRED)
  find_package(scalapack)
  find_package(scotch REQUIRED)
  find_package(MUMPS)
  find_package(pastix REQUIRED)

  include_directories(${MAPHYS_INCLUDE_DIR})

  ## ###################################################################
  ## Build rules
  ## ###################################################################

  add_definitions(-fPIC)
  add_definitions(-DQT_PLUGIN)

  add_library(${PROJECT_NAME} SHARED
    maphysSparseSolver.h
    maphysSparseSolver.cpp
    maphysSparseSolverPlugin.h
    maphysSparseSolverPlugin.cpp)

  ## #################################################################
  ## set layer version in JSON
  ## #################################################################

  configure_file("${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME}.json.in"
                 "${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME}.json")


  ## ###################################################################
  ## Link rules
  ## ###################################################################

  target_link_libraries(${PROJECT_NAME} Qt5::Core)

  target_link_libraries(${PROJECT_NAME} dtkCore)
  target_link_libraries(${PROJECT_NAME} dtkDistributed)
  target_link_libraries(${PROJECT_NAME} dtkLog)
  target_link_libraries(${PROJECT_NAME} dtkLinearAlgebraSparseCore)
  target_link_libraries(${PROJECT_NAME} ${MPI_LIBRARIES})
  target_link_libraries(${PROJECT_NAME} ${MPI_Fortran_LIBRARIES})
  target_link_libraries(${PROJECT_NAME} ${MAPHYS_LIBRARIES})
  target_link_libraries(${PROJECT_NAME}
    ${PASTIX_LIBRARIES}
    ${MUMPS_LIBRARIES}
    ${SCALAPACK_LIBRARIES}
    ${SCOTCH_LIBRARIES}
    ${HWLOC_LIBRARIES})

  ## #################################################################
  ## Install rules
  ## #################################################################

  install(TARGETS ${PROJECT_NAME}
    RUNTIME DESTINATION plugins/${DTK_CURRENT_LAYER}
    LIBRARY DESTINATION plugins/${DTK_CURRENT_LAYER}
    ARCHIVE DESTINATION plugins/${DTK_CURRENT_LAYER})

endif(MAPHYS_FOUND)
######################################################################
### CMakeLists.txt ends here
