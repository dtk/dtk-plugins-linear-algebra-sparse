// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

class maphysSparseSolverPrivate;

class maphysSparseSolver : public dtkSparseSolver<double>
{
public:
     maphysSparseSolver(void);
    ~maphysSparseSolver(void);

public:
    void setMatrix(dtkSparseMatrix<double>* matrix);
    void setRHSVector(dtkVector<double>* rhs_vector);
    void setSolutionVector(dtkVector<double>* sol_vector);

    void setOptionalParameters(const QHash<QString, double>& parameters);
    void setOptionalParameters(const QHash<QString, int>& parameters);

public:
    void run(void);

public:
    dtkVector<double>* solutionVector(void) const;
    const dtkArray<double>& stats(void) const;

protected:
    maphysSparseSolverPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkSparseSolver<double>* maphysSparseSolverCreator(void)
{
    return new maphysSparseSolver();
}

// 
// maphysSparseSolver.h ends here
