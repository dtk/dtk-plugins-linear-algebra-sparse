// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "maphysSparseSolver.h"
#include "maphysSparseSolverPlugin.h"

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

// ///////////////////////////////////////////////////////////////////
// maphysSparseSolverPlugin
// ///////////////////////////////////////////////////////////////////

void maphysSparseSolverPlugin::initialize(void)
{
    dtkLinearAlgebraSparse::solver::pluginFactory<double>().record("maphysSparseSolver", maphysSparseSolverCreator);
}

void maphysSparseSolverPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(maphysSparseSolver)

// 
// maphysSparseSolverPlugin.cpp ends here
