// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:


#pragma once

#include <dtkCore>

#include <dtkSparseSolver.h>

class pastixSparseSolverDirectPlugin : public dtkSparseSolverPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkSparseSolverPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.pastixSparseSolverDirectPlugin" FILE "pastixSparseSolverDirectPlugin.json")

public:
     pastixSparseSolverDirectPlugin(void) {}
    ~pastixSparseSolverDirectPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

// 
// pastixSparseSolverDirectPlugin.h ends here
