// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

class pastixSparseSolverDirectPrivate;

class pastixSparseSolverDirect : public dtkSparseSolver<double>
{
    Q_OBJECT

public:
     pastixSparseSolverDirect(void);
    ~pastixSparseSolverDirect(void);

public:
    void setMatrix(const dtkSparseMatrix<double>& matrix);
    void setRHSVector(const dtkArray<double>& rhs_vector);

    void setOptionalParameters(const dtkArray<double>& parameters);

    void setPreconditioner(const dtkSparsePreconditioner<double>& preconditioner);

public:
    int run(void);

public:
    const dtkArray<double>& solutionVector(void) const;

protected:
    pastixSparseSolverDirectPrivate *d;
};

// 
// pastixSparseSolverDirect.h ends here
