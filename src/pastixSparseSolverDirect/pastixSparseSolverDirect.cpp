// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

//#include <complex>
#undef PLUGIN_HAVE_MPI
#ifdef PLUGIN_HAVE_MPI
#  include <mpi.h>
#else
#  define MPI_Comm int;
#  define MPI_COMM_WORLD 0
#endif

namespace PaStiX {
  extern "C" {
#include <pastix.h>
  }
}
#include "../pastixSparsePreconditioner/pastixSparsePreconditioner.h"
#include "pastixSparseSolverDirect.h"

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

class pastixSparseSolverDirectPrivate
{
public:
    dtkSparseMatrix<double> *matrix;
    dtkArray<double> *rhs_vector;
    dtkArray<double> *solution_vector;

public:
  PaStiX::pastix_data_t * pastix_data;
  MPI_Comm                pastix_comm;
  PaStiX::pastix_int_t    ncol;               /* Size of the matrix  */
  PaStiX::pastix_int_t   *colptr; /* Indexes of first element of each
						 column in row and
						 values */
  PaStiX::pastix_int_t   *rows; /* Row of each element of the
						 matrix */
  double *values; /* Value of each element
						 of the matrix */
  PaStiX::pastix_int_t   *iparm;  /* integer parameters for pastix */
  double                 *dparm;  /* floating parameters for pastix */
  PaStiX::pastix_int_t   *perm; /* Permutation
				   tabular */
  PaStiX::pastix_int_t   *invp; /* Reverse permutation
				   tabular */

  bool is_first_iter;

};

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

pastixSparseSolverDirect::pastixSparseSolverDirect(void) : dtkSparseSolver<double>(), d(new pastixSparseSolverDirectPrivate)
{
  d->pastix_data = NULL;
  d->pastix_comm = MPI_COMM_WORLD;
  d->colptr	 = NULL;
  d->rows	 = NULL;
  d->values	 = NULL;
  d->rhs_vector	 = NULL;
  d->iparm	 = new PaStiX::pastix_int_t[PaStiX::IPARM_SIZE];
  d->dparm	 = new double[PaStiX::DPARM_SIZE];
  d->perm	 = NULL;
  d->invp	 = NULL;
  d->is_first_iter = false;
}

pastixSparseSolverDirect::~pastixSparseSolverDirect(void)
{
  d->matrix     = NULL;
  d->rhs_vector = NULL; 
  // if (d->solution_vector) delete[] d->solution_vector;

  // if (d->colptr) delete[] d->colptr;
  // if (d->rows)   delete[] d->rows;
  // if (d->values) d->values = NULL;
  // if (d->iparm ) delete[] d->iparm;
  // if (d->dparm ) delete[] d->dparm;
  // if (d->perm  ) delete[] d->perm;
  // if (d->invp  ) delete[] d->invp;
  
}

void pastixSparseSolverDirect::setMatrix(const dtkSparseMatrix<double>& matrix)
{
    if (!d->values) {
        d->matrix = &const_cast<dtkSparseMatrix<double>&>(matrix);
        d->ncol   = d->matrix->colCount();
        d->colptr = new PaStiX::pastix_int_t[d->ncol+1];
        d->rows   = new PaStiX::pastix_int_t[d->matrix->nonZeroCount()];
        d->perm   = new PaStiX::pastix_int_t[d->ncol];
        d->invp   = new PaStiX::pastix_int_t[d->ncol];
        d->values = d->matrix->rawData();
        d->is_first_iter = true;
    }
    int iter = 0;
    d->colptr[0] = 1;
    for (qlonglong i = 0; i < d->matrix->rowCount(); ++i) {
        dtkSparseMatrix<double>::const_iterator cit  = d->matrix->cbegin(i);
        dtkSparseMatrix<double>::const_iterator cend = d->matrix->cend(i);
        d->colptr[i+1] = d->colptr[i] + (cend-cit);
        for(;cit != cend; ++cit) {
            qlonglong j = cit.coordinates().second + 1;
            d->rows[iter++]     = j;
        }
    }
}

void pastixSparseSolverDirect::setRHSVector(const dtkArray<double>& rhs_vector)
{
  d->rhs_vector = &const_cast<dtkArray<double>&>(rhs_vector);
  d->solution_vector = new dtkArray<double>(*(d->rhs_vector));
}

void pastixSparseSolverDirect::setOptionalParameters(const dtkArray<double>& parameters)
{
}
void pastixSparseSolverDirect::setPreconditioner(const dtkSparsePreconditioner<double>& preconditioner)
{
  const pastixSparsePreconditioner *prec = &(dynamic_cast<const pastixSparsePreconditioner&>(preconditioner));
  if (prec) {
    d->pastix_data = prec->pastixData();
  }
}

int pastixSparseSolverDirect::run(void)
{
#ifdef PLUGIN_HAVE_MPI
    // -- Init MPI
    int required = MPI_THREAD_MULTIPLE;
    int provided = -1;
    int initialized = 0;

    MPI_Initialized(&initialized);
    if (!initialized)
        MPI_Init_thread(NULL, NULL, required, &provided);
//    int mpi_id; MPI_Comm_rank(MPI_COMM_WORLD, &mpi_id);
#else

#endif    

  /* Init parameters */
    if(!d->pastix_data){
        /* first init of pastix here */
        if (d->is_first_iter) {
            d->iparm[PaStiX::IPARM_MODIFY_PARAMETER] = PaStiX::API_NO;
            d->iparm[PaStiX::IPARM_VERBOSE]          = PaStiX::API_VERBOSE_NOT;
            PaStiX::d_pastix(&(d->pastix_data),
                             d->pastix_comm,
                             d->ncol,
                             d->colptr,
                             d->rows,
                             d->values,
                             d->perm,
                             d->invp,
                             d->solution_vector->data(),
                             1,
                             d->iparm,
                             d->dparm);
            d->is_first_iter = false;
        }
        /* start with ordering */
        d->iparm[PaStiX::IPARM_START_TASK]       = PaStiX::API_TASK_ORDERING;
    }else{
        /* analyze and fatorization must be already done here */
        d->iparm[PaStiX::IPARM_START_TASK]       = PaStiX::API_TASK_SOLVE;
    }

    /* Preprocess, Fact and solve */
    d->iparm[PaStiX::IPARM_TRANSPOSE_SOLVE]  = PaStiX::API_YES;
    d->iparm[PaStiX::IPARM_VERBOSE]          = PaStiX::API_VERBOSE_NOT;
  d->iparm[PaStiX::IPARM_SYM]              = PaStiX::API_SYM_NO;
  d->iparm[PaStiX::IPARM_FACTORIZATION]    = PaStiX::API_FACT_LU;
  d->iparm[PaStiX::IPARM_END_TASK]         = PaStiX::API_TASK_CLEAN;
  PaStiX::d_pastix(&(d->pastix_data),
		   d->pastix_comm,
		   d->ncol,
		   d->colptr,
		   d->rows,
		   d->values,
		   d->perm,
		   d->invp,
		   d->solution_vector->data(),
		   1,
		   d->iparm,
		   d->dparm);
  
  return 1;
}

const dtkArray<double>& pastixSparseSolverDirect::solutionVector(void) const
{
    return *(d->solution_vector);
}

// 
// pastixSparseSolverDirect.cpp ends here
