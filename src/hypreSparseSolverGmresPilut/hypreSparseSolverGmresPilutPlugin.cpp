// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "hypreSparseSolverGmresPilut.h"
#include "hypreSparseSolverGmresPilutPlugin.h"

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

dtkSparseSolver<double>* hypreSparseSolverGmresPilutCreator(void);

// ///////////////////////////////////////////////////////////////////
// hypreSparseSolverGmresPilutPlugin
// ///////////////////////////////////////////////////////////////////

void hypreSparseSolverGmresPilutPlugin::initialize(void)
{
    dtkLinearAlgebraSparse::solver::pluginFactory<double>().record("hypreSparseSolverGmresPilut", hypreSparseSolverGmresPilutCreator);
}

void hypreSparseSolverGmresPilutPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(hypreSparseSolverGmresPilut)

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

dtkSparseSolver<double>* hypreSparseSolverGmresPilutCreator(void)
{
    return new hypreSparseSolverGmresPilut();
}

// 
// hypreSparseSolverGmresPilutPlugin.cpp ends here
