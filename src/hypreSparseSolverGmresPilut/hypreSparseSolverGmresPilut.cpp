// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "hypreSparseSolverGmresPilut.h"

#include <mpi.h>
#include <HYPRE.h>
#include <HYPRE_parcsr_ls.h>
#include <HYPRE_IJ_mv.h>
#include <HYPRE_krylov.h>

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

class hypreSparseSolverGmresPilutPrivate
{
public:
    dtkSparseMatrix<double> *matrix;
    dtkArray<double> *vector_rhs;
    dtkArray<double> *vector_solution;

public:
    int max_iteration_number;
    qreal residual_reduction_order;
};

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

hypreSparseSolverGmresPilut::hypreSparseSolverGmresPilut(void) : dtkSparseSolver<double>(), d(new hypreSparseSolverGmresPilutPrivate)
{
    d->matrix = NULL;
    d->vector_rhs = NULL;
    d->vector_solution = NULL;

    d->max_iteration_number = 50;
    d->residual_reduction_order = 0.00001;
}

hypreSparseSolverGmresPilut::~hypreSparseSolverGmresPilut(void)
{
    if (d->vector_solution)
        delete d->vector_solution;

    delete d;
}

void hypreSparseSolverGmresPilut::setMatrix(const dtkSparseMatrix<double>& matrix)
{
    d->matrix = &const_cast<dtkSparseMatrix<double>&>(matrix);
}

void hypreSparseSolverGmresPilut::setRHSVector(const dtkArray<double>& rhs_vector)
{
    d->vector_rhs = &const_cast<dtkArray<double>&>(rhs_vector);
}

void hypreSparseSolverGmresPilut::setOptionalParameters(const dtkArray<double>&)
{
}

void hypreSparseSolverGmresPilut::setPreconditioner(const dtkSparsePreconditioner<double>&)
{
}

int hypreSparseSolverGmresPilut::run(void)
{
    int initialized = 0;
    int finalized;

    MPI_Initialized(&initialized);
    if (!initialized) {
        MPI_Init(NULL, NULL);
    }

    int mpi_size; MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    int mpi_id;   MPI_Comm_rank(MPI_COMM_WORLD, &mpi_id);

    if(!d->vector_solution && mpi_id == 0)
        d->vector_solution = new dtkArray<double>(d->vector_rhs->size());


    // Conversion to Hypre data structure
    int size = d->matrix->rowCount();

    int begin;
    if (!mpi_id) {
        begin = 0;
    } else {
        begin = (size / mpi_size + 1) * mpi_id;
    }
    int ilower = begin;

    int end;
    if (mpi_id == mpi_size - 1) {
        end = size;
    } else {
        end = (size / mpi_size + 1) * (mpi_id + 1);
    }
    int iupper = end - 1;
    
    int jlower = ilower;
    int jupper = iupper;

    int nrows = iupper - ilower + 1;

    // Matrix
    HYPRE_IJMatrix ij_A;
    HYPRE_ParCSRMatrix par_A;

    int *line  = new int[nrows+1];
    int *rows  = new int[nrows];
    int *ncols = new int[nrows];

    int local_non_zero_coefficients = 0;
    line[0] = 0;
    int ii;
    int id = 0;
    int *cols = new int[d->matrix->nonZeroCount()];
    double *values_A = new double[d->matrix->nonZeroCount()];
    for(int i = 0; i < nrows; ++i) {
        ii = i + ilower;
        rows[i] = ii;
        dtkSparseMatrix<double>::const_iterator cit  = d->matrix->cbegin(i);
        dtkSparseMatrix<double>::const_iterator cend = d->matrix->cend(i);
        for(;cit != cend; ++cit, ++id) {
            ++local_non_zero_coefficients;
            cols[id] = cit.coordinates().second;
            values_A[id] = *cit;
        }
        line[i+1] = local_non_zero_coefficients;
        ncols[i] = line[i+1] - line[i];
    }

    HYPRE_IJMatrixCreate(MPI_COMM_WORLD, ilower, iupper, jlower, jupper, &ij_A);
    HYPRE_IJMatrixSetObjectType(ij_A, HYPRE_PARCSR);
    HYPRE_IJMatrixInitialize(ij_A);
    HYPRE_IJMatrixSetValues(ij_A, nrows, ncols, rows, cols, values_A);
    HYPRE_IJMatrixAssemble(ij_A);
    HYPRE_IJMatrixGetObject(ij_A, (void **) &par_A);

    // RHS, solution
    HYPRE_IJVector ij_b;
    HYPRE_IJVector ij_x;

    HYPRE_ParVector par_b;
    HYPRE_ParVector par_x;

    HYPRE_IJVectorCreate(MPI_COMM_WORLD, jlower, jupper, &ij_b);
    HYPRE_IJVectorCreate(MPI_COMM_WORLD, jlower, jupper, &ij_x);

    HYPRE_IJVectorSetObjectType(ij_b, HYPRE_PARCSR);
    HYPRE_IJVectorSetObjectType(ij_x, HYPRE_PARCSR);

    HYPRE_IJVectorInitialize(ij_b);
    HYPRE_IJVectorInitialize(ij_x);

    dtkArray<double> *rhs = d->vector_rhs;
    double *values_b = new double[nrows];
    for (int i = 0; i < nrows; ++i) {
        values_b[i] = (*rhs)[i + ilower];
    }
    HYPRE_IJVectorSetValues(ij_b, nrows, rows, values_b);

    double *values_x = new double[nrows];
    for (int i = 0; i < nrows; ++i) {
        values_x[i] = 0.0;
    }
    HYPRE_IJVectorSetValues(ij_x, nrows, rows, values_x);

    HYPRE_IJVectorAssemble(ij_b);
    HYPRE_IJVectorAssemble(ij_x);

    HYPRE_IJVectorGetObject(ij_b, (void **) &par_b);
    HYPRE_IJVectorGetObject(ij_x, (void **) &par_x);

    // Solver and preconditioner
    HYPRE_Solver par_solver, precond;

    HYPRE_ParCSRGMRESCreate(MPI_COMM_WORLD, &par_solver);
    
    /* set the GMRES paramaters */
    HYPRE_GMRESSetKDim(par_solver, 50);
    HYPRE_GMRESSetMaxIter(par_solver, d->max_iteration_number);
    HYPRE_GMRESSetTol(par_solver, d->residual_reduction_order);
    // HYPRE_GMRESSetPrintLevel(par_solver, 2);
    
    /* Now set up the PIlut preconditioner and specify any parameters */
    HYPRE_ParCSRPilutCreate(MPI_COMM_WORLD, &precond);

    /* Set the PCG preconditioner */
    HYPRE_GMRESSetPrecond(par_solver, 
                          (HYPRE_PtrToSolverFcn) HYPRE_ParCSRPilutSolve,
                          (HYPRE_PtrToSolverFcn) HYPRE_ParCSRPilutSetup, 
                          precond);

    /* do the setup */
    HYPRE_ParCSRGMRESSetup(par_solver, par_A, par_b, par_x);
    HYPRE_ParCSRGMRESSolve(par_solver, par_A, par_b, par_x);

    /* get the local solution */
    HYPRE_IJVectorGetValues(ij_x, nrows, rows, values_x);

    if (!mpi_id) {
        MPI_Gather(values_x, nrows, MPI_DOUBLE, d->vector_solution->data(), nrows, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    } else {
        MPI_Gather(values_x, nrows, MPI_DOUBLE, NULL, nrows, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    }

    /* clean up */
    HYPRE_ParCSRGMRESDestroy(par_solver);
    HYPRE_ParCSRPilutDestroy(precond);

    //
    HYPRE_ParCSRMatrixDestroy(par_A);
    HYPRE_ParVectorDestroy(par_b);
    HYPRE_ParVectorDestroy(par_x);

    delete[] line;
    delete[] rows;
    delete[] ncols;
    delete[] cols;
    delete[] values_A;
    delete[] values_b;
    delete[] values_x;

    return 1;
}

const dtkArray<double>& hypreSparseSolverGmresPilut::solutionVector(void) const
{
    return *(d->vector_solution);
}

// 
// hypreSparseSolverGmresPilut.cpp ends here
