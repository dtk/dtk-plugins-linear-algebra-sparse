// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

class hypreSparseSolverGmresPilutPrivate;

class hypreSparseSolverGmresPilut : public dtkSparseSolver<double>
{
    Q_OBJECT

public:
     hypreSparseSolverGmresPilut(void);
    ~hypreSparseSolverGmresPilut(void);

public:
    void setMatrix(const dtkSparseMatrix<double>& matrix);
    void setRHSVector(const dtkArray<double>& rhs_vector);

    void setOptionalParameters(const dtkArray<double>& parameters);

    void setPreconditioner(const dtkSparsePreconditioner<double>& preconditioner);

public:
    int run(void);

public:
    const dtkArray<double>& solutionVector(void) const;

protected:
    hypreSparseSolverGmresPilutPrivate *d;
};

// 
// hypreSparseSolverGmresPilut.h ends here
