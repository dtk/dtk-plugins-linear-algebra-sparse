// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:


#pragma once

#include <dtkCore>

#include <dtkSparseSolver.h>

class hypreSparseSolverGmresPilutPlugin : public dtkSparseSolverPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkSparseSolverPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.hypreSparseSolverGmresPilutPlugin" FILE "hypreSparseSolverGmresPilutPlugin.json")

public:
     hypreSparseSolverGmresPilutPlugin(void) {}
    ~hypreSparseSolverGmresPilutPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

// 
// hypreSparseSolverGmresPilutPlugin.h ends here
