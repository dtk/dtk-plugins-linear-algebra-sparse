// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

class hypreSparseSolverGMRESBoomerAMGPrivate;

class hypreSparseSolverGMRESBoomerAMG : public dtkSparseSolver<double>
{
public:
     hypreSparseSolverGMRESBoomerAMG(void);
    ~hypreSparseSolverGMRESBoomerAMG(void);

public:
    void setMatrix(dtkSparseMatrix<double> *matrix);
    void setRHSVector(dtkVector<double> *rhs_vector);
    void setSolutionVector(dtkVector<double> *sol_vector);

    void setOptionalParameters(const QHash<QString, int>& parameters);
    void setOptionalParameters(const QHash<QString, double>& parameters);

public:
    void run(void);

public:
    dtkVector<double> *solutionVector(void) const;
    const dtkArray<double>& stats(void) const;

protected:
    hypreSparseSolverGMRESBoomerAMGPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkSparseSolver<double> *hypreSparseSolverGMRESBoomerAMGCreator(void)
{
    return new hypreSparseSolverGMRESBoomerAMG();
}

//
// hypreSparseSolverGMRESBoomerAMG.h ends here
