// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "hypreSparseSolverGMRESBoomerAMG.h"
#include "hypreSparseSolverGMRESBoomerAMGPlugin.h"

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

// ///////////////////////////////////////////////////////////////////
// hypreSparseSolverGMRESBoomerAMGPlugin
// ///////////////////////////////////////////////////////////////////

void hypreSparseSolverGMRESBoomerAMGPlugin::initialize(void)
{
    dtkLinearAlgebraSparse::solver::pluginFactory<double>().record("hypreSparseSolverGMRESBoomerAMG", hypreSparseSolverGMRESBoomerAMGCreator);
}

void hypreSparseSolverGMRESBoomerAMGPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(hypreSparseSolverGMRESBoomerAMG)

//
// hypreSparseSolverGMRESBoomerAMGPlugin.cpp ends here
