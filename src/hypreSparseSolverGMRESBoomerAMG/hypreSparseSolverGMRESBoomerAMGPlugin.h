// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkCore>

#include <dtkSparseSolver.h>

class hypreSparseSolverGMRESBoomerAMGPlugin : public dtkSparseSolverPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkSparseSolverPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.hypreSparseSolverGMRESBoomerAMGPlugin" FILE "hypreSparseSolverGMRESBoomerAMGPlugin.json")

public:
     hypreSparseSolverGMRESBoomerAMGPlugin(void) {}
    ~hypreSparseSolverGMRESBoomerAMGPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// hypreSparseSolverGMRESBoomerAMGPlugin.h ends here
