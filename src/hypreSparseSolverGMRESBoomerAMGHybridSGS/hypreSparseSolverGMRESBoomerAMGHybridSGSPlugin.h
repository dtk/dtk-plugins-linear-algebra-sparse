// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkCore>

#include <dtkSparseSolver.h>

class hypreSparseSolverGMRESBoomerAMGHybridSGSPlugin : public dtkSparseSolverPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkSparseSolverPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.hypreSparseSolverGMRESBoomerAMGHybridSGSPlugin" FILE "hypreSparseSolverGMRESBoomerAMGHybridSGSPlugin.json")

public:
     hypreSparseSolverGMRESBoomerAMGHybridSGSPlugin(void) {}
    ~hypreSparseSolverGMRESBoomerAMGHybridSGSPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// hypreSparseSolverGMRESBoomerAMGHybridSGSPlugin.h ends here
