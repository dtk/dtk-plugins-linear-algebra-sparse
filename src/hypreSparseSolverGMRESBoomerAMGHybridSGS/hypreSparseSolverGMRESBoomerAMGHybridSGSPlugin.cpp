// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "hypreSparseSolverGMRESBoomerAMGHybridSGS.h"
#include "hypreSparseSolverGMRESBoomerAMGHybridSGSPlugin.h"

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

// ///////////////////////////////////////////////////////////////////
// hypreSparseSolverGMRESBoomerAMGHybridSGSPlugin
// ///////////////////////////////////////////////////////////////////

void hypreSparseSolverGMRESBoomerAMGHybridSGSPlugin::initialize(void)
{
    dtkLinearAlgebraSparse::solver::pluginFactory<double>().record("hypreSparseSolverGMRESBoomerAMGHybridSGS", hypreSparseSolverGMRESBoomerAMGHybridSGSCreator);
}

void hypreSparseSolverGMRESBoomerAMGHybridSGSPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(hypreSparseSolverGMRESBoomerAMGHybridSGS)

//
// hypreSparseSolverGMRESBoomerAMGHybridSGSPlugin.cpp ends here
