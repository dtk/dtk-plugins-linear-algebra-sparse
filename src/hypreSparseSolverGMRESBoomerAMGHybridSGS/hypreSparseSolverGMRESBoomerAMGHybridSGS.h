// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>
#include <dtkLinearAlgebraSparse>

class hypreSparseSolverGMRESBoomerAMGHybridSGSPrivate;

class hypreSparseSolverGMRESBoomerAMGHybridSGS : public dtkSparseSolver<double>
{
public:
     hypreSparseSolverGMRESBoomerAMGHybridSGS(void);
    ~hypreSparseSolverGMRESBoomerAMGHybridSGS(void);

public:
    void setMatrix(dtkSparseMatrix<double> *matrix);
    void setRHSVector(dtkVector<double> *rhs_vector);
    void setSolutionVector(dtkVector<double> *sol_vector);

    void setOptionalParameters(const QHash<QString, int>& parameters);
    void setOptionalParameters(const QHash<QString, double>& parameters);

public:
    void run(void);

public:
    dtkVector<double> *solutionVector(void) const;
    const dtkArray<double>& stats(void) const;

protected:
    hypreSparseSolverGMRESBoomerAMGHybridSGSPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkSparseSolver<double> *hypreSparseSolverGMRESBoomerAMGHybridSGSCreator(void)
{
    return new hypreSparseSolverGMRESBoomerAMGHybridSGS();
}

//
// hypreSparseSolverGMRESBoomerAMGHybridSGS.h ends here
